{-# OPTIONS_GHC -Wno-type-defaults -Wno-deprecations #-}

module Main where

import RIO

import Data.Bits
import Engine.UI.Layout qualified as Layout
import Geomancy
import Geometry.Quad (Quad(..))
import RIO.Map qualified as Map
import RIO.Set qualified as Set
import Test.Tasty (testGroup)
import Test.Tasty qualified as Tasty
import Test.Tasty.HUnit
import Text.Show.Pretty

import Data.Neighbors qualified as Neighbors

import Resource.Image.Atlas qualified as Atlas
import Layout.Linear qualified as Linear

main :: IO ()
main = Tasty.defaultMain $
  testGroup "¡Hola!"
    [ testAtlas
    , testNeighbors
    , testLayout
    ]

testAtlas :: Tasty.TestTree
testAtlas = testGroup "Atlas"
  [ testGroup "fromTileSize"
      [ testCase "1-tile size, zero margin" $
          Atlas.fromTileSize 1 16 0 @?=
            Atlas.Atlas
              { sizeTiles  = 1
              , sizePx     = 16
              , tileSizePx = 16
              , marginPx   = 0
              , uvScale    = 16/16
              }
      , testCase "1-tile size, 1px margin" $
          Atlas.fromTileSize 1 16 1 @?=
            Atlas.Atlas
              { sizeTiles  = 1
              , sizePx     = 16
              , tileSizePx = 16
              , marginPx   = 1
              , uvScale    = 16/16
              }

      , testCase "2-tile size, zero margin" $
          Atlas.fromTileSize 2 16 0 @?=
            Atlas.Atlas
              { sizeTiles  = 2
              , sizePx     = 32
              , tileSizePx = 16
              , marginPx   = 0
              , uvScale    = 16/32
              }

      , testCase "2-tile size, 1px margin" $
          Atlas.fromTileSize 2 16 1 @?=
            Atlas.Atlas
              { sizeTiles  = 2
              , sizePx     = 33
              , tileSizePx = 16
              , marginPx   = 1
              , uvScale    = 16 / 33
              }
      ]
  , testGroup "fromImageSize"
      [ testCase "1-tile size, no margin" $
          Atlas.fromImageSize 16 16 0 @?=
            Right Atlas.Atlas
              { sizeTiles  = 1
              , sizePx     = 16
              , tileSizePx = 16
              , marginPx   = 0
              , uvScale    = 16/16
              }
      , testCase "1-tile size, no margin, leftovers" $
          Atlas.fromImageSize 18 16 0 @?=
            Left 2

      , testCase "1-tile size, 1px margin" $
          Atlas.fromImageSize 16 16 1 @?=
            Right Atlas.Atlas
              { sizeTiles  = 1
              , sizePx     = 16
              , tileSizePx = 16
              , marginPx   = 1
              , uvScale    = 16/16
              }
      , testCase "2-tile size, 1px margin" $
          Atlas.fromImageSize 33 16 1 @?=
            Right Atlas.Atlas
              { sizeTiles  = 2
              , sizePx     = 33
              , tileSizePx = 16
              , marginPx   = 1
              , uvScale    = 16/33
              }
      , testCase "5x3-tile size, 1px margin" $
          Atlas.fromImageSize (uvec2 84 50) 16 1 @?=
            Right Atlas.Atlas
              { sizeTiles  = uvec2 5 3
              , sizePx     = uvec2 84 50
              , tileSizePx = 16
              , marginPx   = 1
              , uvScale    = 16 / vec2 84 50
              }
      ]
  ]

testNeighbors :: Tasty.TestTree
testNeighbors = testGroup "Neighbors"
  [ testGroup "bits"
      [ testCase "all" do
          Neighbors.fromBitsNW @Int 0b00000000 @?= Neighbors.nobody
          Neighbors.fromBitsNW @Int 0b11111111 @?= Neighbors.everyone

          Neighbors.toBitsNW Neighbors.everyone @?= 0b11111111
          Neighbors.toBitsNW Neighbors.nobody   @?= 0b00000000
      ]
  , testGroup "microblob"
      [ testCase "getSets" do
          let (uniq, dups) = Neighbors.getSets Neighbors.bitsNW
          let dupsUniq = Set.fromList $ Map.elems dups
          uniq @=? dupsUniq

      , testCase "quads" do
          let (uniq, _dups) = Neighbors.getSets Neighbors.bitsNW
          traceShowM uniq
          for_ uniq \index ->
            traceM . fromString . ppShow $
              Neighbors.microblobQuad Neighbors.bitsNW Neighbors.microblobNames index

          Neighbors.microblobQuad Neighbors.bitsNW Neighbors.microblobNames (complement 16) @=?
            Quad
              "full" "full"
              "full" "brCornerInner"
      ]
  ]

testLayout :: Tasty.TestTree
testLayout =
  testGroup "Layout"
    [ testGroup "linear"
        [ testGroup "Ranges"
            [ testCase "empty" $
                Linear.ranges [] @?= (0, [])

            , testCase "single" $
                Linear.ranges [1] @?= (1, [(0, 1)])

            , testCase "double, equal items" $
                Linear.ranges [1, 1] @?= (2, [(0, 1), (1, 2)])
            , testCase "double, different items" $
                Linear.ranges [1, 2] @?= (3, [(0, 1), (1, 3)])

            , testCase "triple, equal items" $
                Linear.ranges [1, 1, 1] @?= (3, [(0, 1), (1, 2), (2, 3)])
            , testCase "triple, different items" $
                Linear.ranges [1, 2, 3] @?= (6, [(0, 1), (1, 3), (3, 6)])
            ]

        , testGroup "Midpoints"
            [ testCase "empty" $
                Linear.ranges [] @?= (0, [])

            , testCase "single" $
                (Linear.midpoints . snd . Linear.ranges) [1] @?= [0.5]

            , testCase "double, equal items" $
                (Linear.midpoints . snd . Linear.ranges) [1, 1] @?= [0.5, 1.5]
            , testCase "double, different items" $
                (Linear.midpoints . snd . Linear.ranges) [1, 2] @?= [0.5, 2.0]

            , testCase "triple, equal items" $
                (Linear.midpoints . snd . Linear.ranges) [1, 1, 1] @?= [0.5, 1.5, 2.5]
            , testCase "triple, different items" $
                (Linear.midpoints . snd . Linear.ranges) [1, 2, 3] @?= [0.5, 2.0, 4.5]
            ]

        , testGroup "Place"
            [ testCase "Fits" do
                Linear.place 0.0 1 10 @?= (0, 9)
                Linear.place 1.0 1 10 @?= (9, 0)
                Linear.place 0.5 1 10 @?= (4.5, 4.5)
            , testCase "Oversize" do
                Linear.place 0.0 2 1 @?= (0, -1)
                Linear.place 1.0 2 1 @?= (-1, 0)
                Linear.place 0.5 2 1 @?= (-0.5, -0.5)
            ]

        , testGroup "Boxes"
            [ testGroup "Center"
                let
                  parent = Layout.Box 0 100
                in
                  [ testGroup "Horizontal"
                      [ testCase "empty" $
                          Linear.hBoxShares [] parent @?= []

                      , testCase "single" $
                          Linear.hBoxShares [1] parent @?= [parent]

                      , testCase "double, equal shares" $
                          Linear.hBoxShares [1, 1] parent @?=
                            [ Layout.Box
                                { boxPosition = vec2 (-25) 0
                                , boxSize     = vec2 50 100
                                }
                            , Layout.Box
                                { boxPosition = vec2 25 0
                                , boxSize     = vec2 50 100
                                }
                            ]
                      , testCase "double, different shares" $
                          Linear.hBoxShares [1, 3] parent @?=
                            [ Layout.Box
                                { boxPosition = vec2 (-37.5) 0
                                , boxSize     = vec2 25 100
                                }
                            , Layout.Box
                                { boxPosition = vec2 12.5 0
                                , boxSize     = vec2 75 100
                                }
                            ]
                      ]
                  , testGroup "Vertical"
                      [ testCase "empty" $
                          Linear.vBoxShares [] parent @?= []

                      , testCase "single" $
                          Linear.vBoxShares [1] parent @?= [parent]

                      , testCase "double, equal shares" $
                          Linear.vBoxShares [1, 1] parent @?=
                            [ Layout.Box
                                { boxPosition = vec2 0 (-25)
                                , boxSize     = vec2 100 50
                                }
                            , Layout.Box
                                { boxPosition = vec2 0 25
                                , boxSize     = vec2 100 50
                                }
                            ]
                      , testCase "double, different shares" $
                          Linear.vBoxShares [1, 3] parent @?=
                            [ Layout.Box
                                { boxPosition = vec2 0 (-37.5)
                                , boxSize     = vec2 100 25
                                }
                            , Layout.Box
                                { boxPosition = vec2 0 12.5
                                , boxSize     = vec2 100 75
                                }
                            ]
                      ]
                  , testGroup "Place"
                      [ testGroup "Same size"
                          [ testCase "left-top" $
                              Linear.placeBox (vec2 0.0 0.0) 100 parent @?= parent
                          , testCase "right-top" $
                              Linear.placeBox (vec2 1.0 0.0) 100 parent @?= parent
                          , testCase "center" $
                              Linear.placeBox (vec2 0.5 0.5) 100 parent @?= parent
                          ]
                      , testGroup "Fits"
                          [ testCase "left-top" $
                              Linear.placeBox (vec2 0.0 0.0) 50 parent @?= Layout.Box
                                { boxPosition = vec2 (-25) (-25)
                                , boxSize     = 50
                                }
                          , testCase "right-top" $
                              Linear.placeBox (vec2 1.0 0.0) 50 parent @?= Layout.Box
                                { boxPosition = vec2 25 (-25)
                                , boxSize     = 50
                                }
                          , testCase "right-bottom" $
                              Linear.placeBox (vec2 1.0 1.0) 50 parent @?= Layout.Box
                                { boxPosition = vec2 25 25
                                , boxSize     = 50
                                }
                          , testCase "center" $
                              Linear.placeBox (vec2 0.5 0.5) 50 parent @?= Layout.Box
                                { boxPosition = vec2 0 0
                                , boxSize     = 50
                                }
                          ]
                      , testGroup "Oversize"
                          [ testCase "center" $
                              Linear.placeBox (vec2 0.5 0.5) 200 parent @?= Layout.Box
                                { boxPosition = vec2 0 0
                                , boxSize     = 200
                                }
                          , testCase "left-top" $
                              Linear.placeBox (vec2 0.0 0.0) 200 parent @?= Layout.Box
                                { boxPosition = vec2 50 50
                                , boxSize     = 200
                                }
                          , testCase "right-top" $
                              Linear.placeBox (vec2 1.0 0.0) 200 parent @?= Layout.Box
                                { boxPosition = vec2 (-50) 50
                                , boxSize     = 200
                                }
                          , testCase "right-bottom" $
                              Linear.placeBox (vec2 1.0 1.0) 200 parent @?= Layout.Box
                                { boxPosition = vec2 (-50) (-50)
                                , boxSize     = 200
                                }
                          ]
                      ]
                  ]

            , testGroup "Shifted"
                let
                  parent = Layout.Box 100 100
                in
                  [ testGroup "Horizontal"
                      [ testCase "empty" $
                          Linear.hBoxShares [] parent @?= []

                      , testCase "single" $
                          Linear.hBoxShares [1] parent @?= [parent]

                      , testCase "double, equal shares" $
                          Linear.hBoxShares [1, 1] parent @?=
                            [ Layout.Box
                                { boxPosition = vec2 75 100
                                , boxSize     = vec2 50 100
                                }
                            , Layout.Box
                                { boxPosition = vec2 125 100
                                , boxSize     = vec2 50 100
                                }
                            ]
                      , testCase "double, different shares" $
                          Linear.hBoxShares [1, 3] parent @?=
                            [ Layout.Box
                                { boxPosition = vec2 62.5 100
                                , boxSize     = vec2 25 100
                                }
                            , Layout.Box
                                { boxPosition = vec2 112.5 100
                                , boxSize     = vec2 75 100
                                }
                            ]
                      ]
                  , testGroup "Vertical"
                      [ testCase "empty" $
                          Linear.vBoxShares [] parent @?= []

                      , testCase "single" $
                          Linear.vBoxShares [1] parent @?= [parent]

                      , testCase "double, equal shares" $
                          Linear.vBoxShares [1, 1] parent @?=
                            [ Layout.Box
                                { boxPosition = vec2 100 75
                                , boxSize     = vec2 100 50
                                }
                            , Layout.Box
                                { boxPosition = vec2 100 125
                                , boxSize     = vec2 100 50
                                }
                            ]
                      , testCase "double, different shares" $
                          Linear.vBoxShares [1, 3] parent @?=
                            [ Layout.Box
                                { boxPosition = vec2 100 62.5
                                , boxSize     = vec2 100 25
                                }
                            , Layout.Box
                                { boxPosition = vec2 100 112.5
                                , boxSize     = vec2 100 75
                                }
                            ]
                      ]
                  ]
            ]

        -- , testCase "Example" do
        --         let
        --           box = Layout.Box
        --             { boxPosition = 0
        --             , boxSize = 100
        --             }

        --           sizes = [1, 1]
        --         Linear.hBoxShares sizes box @?= [Layout.Box 0 0]
        --         --   (final, ranges) = Linear.ranges sizes
        --         --   mids = Linear.midpoints ranges
        --         -- final @?= 4
        --         -- zip mids sizes @?= []
        ]
    ]
