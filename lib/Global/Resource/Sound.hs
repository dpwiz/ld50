{-# LANGUAGE DeriveAnyClass #-}

-- {-# OPTIONS_GHC -fforce-recomp #-}

module Global.Resource.Sound
  ( Collection(..)
  , Sources
  , configs
  , indices
  ) where

import RIO

import Resource.Collection (Generic1, Generically1(..), enumerate)
import Resource.Opus qualified as Opus
import Resource.Source qualified as Source
import Resource.Static qualified as Static

data Collection a = Collection
  { bang_large  :: a
  , bang_medium :: a
  , bang_small  :: a
  }
  deriving stock (Show, Functor, Foldable, Traversable, Generic1)
  deriving Applicative via Generically1 Collection

type Sources = Collection Opus.Source

Static.filePatterns Static.Files "data/sound"

configs :: Collection Opus.Config
configs = Collection
  { bang_large = Opus.Config
      { gain        = 1.0
      , loopingMode = False
      , byteSource  = Source.File Nothing BANG_LARGE_OPUS
      }

  , bang_medium = Opus.Config
      { gain        = 1.0
      , loopingMode = False
      , byteSource  = Source.File Nothing BANG_MEDIUM_OPUS
      }

  , bang_small = Opus.Config
      { gain        = 1.0
      , loopingMode = False
      , byteSource  = Source.File Nothing BANG_SMALL_OPUS
      }
  }

indices :: Collection Int
indices = fmap fst $ enumerate configs
