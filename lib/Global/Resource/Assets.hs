{-# LANGUAGE OverloadedLists #-}

module Global.Resource.Assets
  ( Assets(..)
  , load
  ) where

import RIO.Local

-- import Geomancy (vec2, vec3)
-- import Render.Lit.Colored.Model qualified as LitColored
-- import Render.Lit.Material.Collect (SceneModel(..))
-- import Resource.Mesh.Codec qualified as Mesh
-- import Vulkan.Utils.Debug qualified as Debug
import Engine.Sound.Device qualified as SoundDevice
import Engine.Sound.Source qualified as SoundSource
import Engine.Vulkan.Types (MonadVulkan, Queues)
import Geomancy.Vec3 qualified as Vec3
import Geomancy.Vec4 qualified as Vec4
import Geometry.Cube qualified as Cube
import Geometry.Icosphere qualified as Icosphere
import Geometry.Quad qualified as Quad
import Render.Lit.Material.Collect qualified as Collect
import Render.Unlit.Colored.Model qualified as UnlitColored
import Resource.Buffer qualified as Buffer
import Resource.Collection qualified as Collection
import Resource.Combined.Textures qualified as CombinedTextures
import Resource.CommandBuffer (withPools)
import Resource.Font qualified as Font
import Resource.Model qualified as Model
import Resource.Opus qualified as Opus
import Resource.Region qualified as Region
import Resource.Texture.Ktx2 qualified as Ktx2
import UnliftIO.Resource (MonadResource)
import Vulkan.Core10 qualified as Vk

import Global.Resource.Combined (CombinedCollection)
import Global.Resource.CubeMap qualified as CubeMap
import Global.Resource.Font (FontCollection)
import Global.Resource.Font qualified as GameFont
import Global.Resource.Material (MaterialCollection)
import Global.Resource.Model qualified as GameModel
import Global.Resource.Sound qualified as Sound
import Global.Resource.Texture qualified as Texture

data Assets = Assets
  { aFonts     :: FontCollection
  , aTextures  :: Texture.TextureCollection
  , aCombined  :: CombinedCollection
  , aCubeMaps  :: CubeMap.Textures
  , aModels    :: GameModel.Collection
  , aMaterials :: MaterialCollection
  , aSounds    :: Sound.Collection Opus.Source
  }

load
  :: ( MonadVulkan env m
     , HasLogFunc env
     , MonadThrow m
     , MonadResource m
     )
  => (Text -> m ())
  -> m Assets
load update' = Region.eval do
  Region.logDebug
    "Loading global assets"
    "Releasing global assets"

  let update = lift . update'
  update "Loading sounds"
  soundDevice <- Region.local SoundDevice.allocate
  !aSounds <- Region.local $
    SoundSource.allocateCollectionWith
      (Opus.load soundDevice)
      Sound.configs

  -- SoundSource.play (Sound.bg aSounds)
  -- let
  --   update msg = do
  --     SoundSource.play (Sound.detect sounds)
  --     update' msg

  withPools \pools -> do
    update "Loading textures"
    -- Batch 1
    fonts <- traverse (Font.allocate pools) GameFont.configs
    textures <- traverse (Ktx2.load pools) Texture.sources

    let
      aCombined = Collection.enumerate CombinedTextures.Collection
        { textures = textures
        , fonts    = fmap Font.texture fonts
        }

      aFonts =
        (,)
          <$> fmap fst (CombinedTextures.fonts aCombined)
          <*> fmap Font.container fonts

      aTextures =
        fmap fst $ CombinedTextures.textures aCombined

    -- Batch 2
    update "Loading cubes"
    aCubeMaps <- traverse (Ktx2.load pools) CubeMap.sources

    -- Batch 3
    aModels <- loadModels pools update
    aMaterials <- collectMaterials update aModels aTextures

    update "Assets loaded"
    pure Assets{..}

loadModels
  :: ( MonadResource m
     , MonadVulkan env m
    --  , HasLogFunc env
     )
  => Queues Vk.CommandPool
  -> (Text -> ResourceT m ())
  -> ResourceT m GameModel.Collection
loadModels pools updateProgress = do
  updateProgress "Loading models"

  bbWire <- Model.createStagedL (Just "bbWire") pools Cube.bbWireColored Nothing
  Model.registerIndexed_ bbWire

  quadUV <- Model.createStagedL (Just "quadUV") pools (Quad.toVertices Quad.texturedQuad) Nothing
  Model.registerIndexed_ quadUV

  icosphere1 <- allocateSphere pools 1
  icosphere4 <- allocateSphere pools 4

  tileCursorWire <- Model.createStagedL (Just "tileCursorWire") pools
    (toList $ Quad.coloredQuad UnlitColored.white)
    (Just Quad.indicesWire)

  tileCursorQuad <- Model.createStagedL (Just "tileCursorQuad") pools
    (toList $ Quad.coloredQuad UnlitColored.white)
    (Just Quad.indicesQuad)

  zeroTransform <-
    Buffer.createStaged
      (Just "zeroTransform")
      pools
      Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT
      1
      [mempty]
  Buffer.register zeroTransform

  -- updateProgress "data/models/craft_racer.weld3"
  -- craftRacer <- Region.local $
  --   Mesh.loadIndexed pools "data/models/craft_racer.weld3"

  pure GameModel.Collection{..}

collectMaterials
  :: Monad m
  => (Text -> ResourceT m ())
  -> GameModel.Collection
  -> Texture.TextureCollection
  -> ResourceT m MaterialCollection
collectMaterials updateProgress loadedModels textures = do
  updateProgress "Collecting materials..."

  pure $
    Collect.sceneMaterials
      loadedModels
      textures
      Nothing

allocateSphere
  :: MonadVulkan context m
  => Queues Vk.CommandPool
  -> Natural
  -> ResourceT m (UnlitColored.Model 'Buffer.Staged)
allocateSphere pools details = do
  model <- Model.createStaged (Just "sphere") pools pv av iv
  Model.registerIndexed_ model
  pure model
  where
    (pv, av, iv) =
      Icosphere.generateIndexed
        details
        mkInitialAttrs
        mkMidpointAttrs
        mkVertices

    mkInitialAttrs _pos = ()

    mkMidpointAttrs (_scale :: Float) _midPos _attr1 _attr2 = ()
      -- Vec3.lerp
      --   (scale * scale)
      --   (hash33 midPos)
      --   (Vec3.lerp 0.5 attr1 attr2)

    mkVertices points _faces = do
      (rawPos, ()) <- points
      let normPos = Vec3.normalize rawPos

      pure
        ( Vec3.Packed normPos ^* 100
        , Vec4.fromVec3 (normPos * v05 + v05) 1
          -- LitColored.VertexAttrs
          --   { vaBaseColor         = Vec4.fromVec3 (normPos * v05 + v05) 1
          --   , vaEmissiveColor     = 0.1
          --   , vaMetallicRoughness = vec2 0.5 0.5
          --   , vaNormal            = Vec3.Packed normPos
          --   }
        )

    v05 = vec3 0.5 0.5 0.5
