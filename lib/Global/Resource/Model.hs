module Global.Resource.Model where

import RIO.Local

-- import RIO.Vector.Storable qualified as Storable

-- import Render.Lit.Colored.Model qualified as LitColored
import Render.Unlit.Colored.Model qualified as UnlitColored
import Render.Unlit.Textured.Model qualified as UnlitTextured
import Resource.Buffer qualified as Buffer
-- import Resource.Mesh.Types qualified as Mesh

data Collection = Collection
  { bbWire     :: UnlitColored.Model 'Buffer.Staged
  , quadUV     :: UnlitTextured.Model 'Buffer.Staged
  , zeroTransform :: Buffer.Allocated 'Buffer.Staged Transform

  , icosphere1 :: UnlitColored.Model 'Buffer.Staged
  , icosphere4 :: UnlitColored.Model 'Buffer.Staged

  , tileCursorWire :: UnlitColored.Model 'Buffer.Staged
  , tileCursorQuad :: UnlitColored.Model 'Buffer.Staged

  -- , craftRacer :: LoadedModel
  }

-- type LoadedModel =
--   ( Mesh.Meta
--   , Storable.Vector Mesh.Node
--   , UnlitColored.Model 'Buffer.Staged
--   )
