{-# LANGUAGE DeriveAnyClass #-}

-- {-# OPTIONS_GHC -fforce-recomp #-}

module Global.Resource.Texture
  ( Collection(..)
  , TextureCollection
  , sources
  ) where

import RIO

import Resource.Collection (Generic1, Generically1(..))
import Resource.Source (Source)
import Resource.Static qualified as Static
import Resource.Source qualified as Source

import Global.Resource.Texture.Base qualified as Base

data Collection a = Collection
  { base :: Base.Collection a

  , medieval :: a
  , nebula :: a
  }
  deriving stock (Show, Functor, Foldable, Traversable, Generic1)
  deriving Applicative via Generically1 Collection

type TextureCollection = Collection Int32

Static.filePatterns Static.Files "data/textures"

sources :: Collection Source
sources = Collection
  { base = Base.sources

  , medieval = Source.File Nothing MEDIEVAL_KTX2
  , nebula = Source.File Nothing NEBULA_KTX2
  }
