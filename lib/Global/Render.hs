module Global.Render where

import RIO.Local

-- import Engine.Camera.Controls qualified as CameraControls
-- import RIO.State (modify')
-- import Stage.Loader.Setup qualified as Loader
import Engine.Types (StageRIO)
import Engine.Types qualified as Engine
import Engine.Vulkan.Swapchain qualified as Swapchain
import Render.Basic qualified as Basic
import Render.DescSets.Set0 qualified as Set0
import Render.ImGui qualified as ImGui
import Render.Samplers qualified as Samplers
-- import Data.Tagged (Tagged(..))

-- import Stage.MainMenu.Events qualified as Events
-- import Stage.MainMenu.Reactive.Stuff qualified as StuffNetwork
-- import Stage.MainMenu.World.Mesh qualified as Mesh
import Global.Resource.Assets qualified as Global
-- import Render.Unlit.Sprite.Pipeline qualified as Sprite

type Stage = Engine.Stage Basic.RenderPasses Pipelines

type StageFrameRIO rs fr = Engine.StageFrameRIO Basic.RenderPasses Pipelines rs fr

data Pipelines = Pipelines
  { pBasic  :: Basic.Pipelines
  -- , pSprite :: Sprite.Pipeline
  }

allocatePipelines
  :: Swapchain.HasSwapchain swapchain
  => Global.Assets
  -> swapchain
  -> Basic.RenderPasses
  -> ResourceT (StageRIO st) Pipelines
allocatePipelines assets swapchain rps = do
  void $! ImGui.allocate swapchain (Basic.rpForwardMsaa rps) 0

  let msaa = Swapchain.getMultisample swapchain

  samplers <- Samplers.allocate (Swapchain.getAnisotropy swapchain)
  let
    sceneBinds =
      Set0.mkBindings
        samplers
        (Global.aCombined assets)
        (Global.aCubeMaps assets)
        0

  pBasic <- Basic.allocatePipelines sceneBinds msaa rps

  -- pSprite <- Sprite.allocate Nothing msaa sceneBinds (Basic.rpForwardMsaa rps)

  pure Pipelines{..}
