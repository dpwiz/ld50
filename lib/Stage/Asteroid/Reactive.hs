{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE RecursiveDo #-}

module Stage.Asteroid.Reactive
  ( allocate
  ) where

import RIO.Local

import Data.Vector.Storable qualified as Storable
import Engine.Reactive.Collider qualified as Collider
import Engine.Reactive.Motion qualified as Motion
import Engine.ReactiveBanana qualified as Network
import Engine.ReactiveBanana.Course qualified as Course
import Engine.ReactiveBanana.Window qualified as Window
import Engine.Sound.Source qualified as SoundSource
import Engine.StageSwitch (trySwitchStage)
import Engine.Types (StageRIO)
import Engine.Types qualified as Engine
import Engine.Window.CursorPos qualified as CursorPos
import Engine.Window.MouseButton qualified as MouseButton
import Reactive.Banana ((<@>), (@>))
import Reactive.Banana qualified as RB
import Reactive.Banana.Frameworks qualified as RBF
import Render.Unlit.Sprite.Model qualified as Sprite
import Resource.Image.Atlas qualified as Atlas
import RIO.State (get)

import Common.Reactive (dtFast)
import Common.Reactive qualified as Common
import Common.Reactive.Sparks qualified as Sparks
import Global.Resource.Assets qualified as Global
import Global.Resource.Sound qualified as Sound
import Global.Resource.Texture qualified as Texture
import Stage.Asteroid.Reactive.Asteroids qualified as Asteroids
import Stage.Asteroid.Reactive.Bullets qualified as Bullets
import Stage.Asteroid.Types (RunState(..))

allocate :: ResourceT (StageRIO RunState) RBF.EventNetwork
allocate = do
  fromCursorPos <- Network.eventHandler $ CursorPos.callback . curry
  fromMouseButton <- Network.eventHandler MouseButton.callback
  fromTimerDebug <- Network.timer 1e6
  fromTimerFast <- Network.timer $ round (1e6 * dtFast)
  fromTimerShoot <- Network.timer $ round @Float (1e6 / 5.0)
  mkTimerAsteroid <- Motion.spawnTimer dtFast
  mkTimerBullet <- Motion.spawnTimer dtFast
  mkTimerSparks <- Motion.spawnTimer dtFast

  RunState{..} <- get
  let Global.Assets{aSounds, aTextures} = rsGlobalAssets
  maxBulletsVar <- Network.observe rsMaxBullets

  tilesetAtlas <-
    case Atlas.fromImageSize (uvec2 112 832) 16 0 of
      Left _overs ->
        throwString "Invalid tile atlas size"
      Right ok ->
        pure ok
  let
    tilesetAt scale (WithIVec2 atlasX atlasY) =
      Sprite.fromAtlas
        (Texture.medieval aTextures)
        tilesetAtlas
        scale
        (vec2 (fromIntegral atlasX) (fromIntegral atlasY))

  Network.allocateActuated \(UnliftIO unlift) started -> mdo
    fastTickE <- fromTimerFast

    -------------------

    -- XXX: screen-sized layout base
    screenBox <- Window.setupScreenBox (liftIO . unlift)

    -------------------

    -- XXX: project window cursor position to layout
    (_cursorPosE, cursorPos) <- Window.setupCursorPos fromCursorPos screenBox

    -------------------

    mouseClicked' <- Window.setupMouseClicks fromMouseButton cursorPos
    -- for_ (zip (toList MouseButton.collectionGlfw) (toList mouseClicked)) \(btn, clickE) ->
    --   reactimateDebugShow $ fmap (btn,) clickE

    let
      mouseClicked =
        fmap (Course.whenIdle gameStopping) mouseClicked'

    -------------------

    -- XXX: fade-in
    (gameStartingE, gameStartedE, _gameStarting) <-
      Course.setup (started $> 1.0) $
        fastTickE $> \old ->
          if old <= dtFast then
            Left Nothing
          else
            Right (old - dtFast * 2)
    Network.pushWorkerInput rsShroud $ fmap Just gameStartingE
    Network.pushWorkerInput rsShroud gameStartedE

    -- XXX: fade-out
    (gameStoppingE, gameStoppedE, gameStopping) <-
      Course.setup (MouseButton.mb1 mouseClicked $> 1.0) $
        fastTickE $> \old ->
          if old <= dtFast then
            Left ()
          else
            Right (old - dtFast)
    Network.pushWorkerInput rsShroud $ fmap (Just . (1.0 -)) gameStoppingE
    RBF.reactimate $ gameStoppedE $>
      unlift (void $ trySwitchStage Engine.Finish)

    -------------------

    Network.pushWorkerOutput rsNebula $
      fastTickE <&> Common.animateNebula aTextures

    -------------------------------------

    asteroidTickE <- mkTimerAsteroid

    asteroidPositions <- Asteroids.setup
      asteroidsSpawnE
      asteroidsHitE

    asteroidsSpawnE <- Asteroids.autoSpawn asteroidTickE asteroidPositions dtFast

    -------------------------------------

    autoShootE <-
      fromTimerShoot <&>
        RB.filterApply do
          num <- numBullets
          cur <- cursorPos
          limit <- maxBullets
          pure $ const $
            withVec2 cur \x y ->
              not $
                isInfinite x ||
                isInfinite y ||
                num > limit

    let
      numBullets = bulletPositions <&> length

    maxBullets <- maxBulletsVar >>= RB.stepper 0

    let
      fireBulletsE =
        -- MouseButton.mb2 mouseClicked <&> \(_mods, pos) ->
        autoShootE @> cursorPos <&> \pos ->
          let
            -- distance = sqrt (quadrance pos)
            direction = normalize pos
            normal = withVec2 direction \x y -> vec2 y (-x)
          in
            [ Bullets.New
                { params =
                    ivec2 3 42
                , fuse =
                    5
                , position =
                    Motion.dirSpeed
                      bulletTickE
                      (Point 0)
                      normal
                      (10 * 16)
                }
            , Bullets.New
                { params =
                    ivec2 0 43
                , fuse =
                    5
                , position =
                    Motion.homing
                      bulletTickE
                      (Point 0)
                      (fmap Point cursorPos)
                      (10 * 16)
                }
            , Bullets.New
                { params =
                    ivec2 4 42
                , fuse =
                    5
                , position = do
                    a <- Motion.dirSpeed
                      bulletTickE
                      (Point 0)
                      normal
                      (10 * 16)

                    b <- Motion.homing
                      bulletTickE
                      (Point 0)
                      (fmap Point cursorPos)
                      (10 * 16)

                    t <- RB.accumB (-2) $
                      bulletTickE <&> \(Motion.ClockTick dt) ->
                        (+ dt)

                    pure $ Motion.mixLinear a b t
                }
            ]

    bulletTickE <- mkTimerBullet

    bulletPositions <- Bullets.setup
      bulletTickE
      fireBulletsE
      bulletsHitE

    -------------------------------------

    let
      (asteroidsHitE, bulletsHitE, hitsE) =
        Collider.setup
          fastTickE
          asteroidPositions
          bulletPositions

    -------------------------------------

    sparkTickE <- mkTimerSparks
    sparkTime <- RBF.fromPoll getMonotonicTime
    sparkPositions <- Sparks.setup
      sparkTickE
      hitSparksE

    let
      hitSparksE = go <$> sparkTime <@> hitsE
        where
          go seed hits = do
            (ix, (a, _b)) <- zip [0..] hits
            let (_ixA, posA, _paramsA) = a
            Sparks.explode sparkTickE (seed + ix * 1337) posA

    -------------------------------------

    RBF.reactimate $ hitsE <&> \_hits -> do
      seed <- getMonotonicTime
      SoundSource.play $
        Common.animFrame
          [ Sound.bang_large aSounds
          , Sound.bang_medium aSounds
          , Sound.bang_small aSounds
          ]
          100500
          seed

    -------------------------------------

    Network.pushWorkerOutput rsSprites $ bulletTickE @> do
      bullets <- bulletPositions
      activeAsteroids <- asteroidPositions
      sparks <- sparkPositions
      pure $ Storable.fromList $
        let
          as = do
            (Asteroids.Params{sprite, size}, Point position) <- activeAsteroids
            pure $ tilesetAt (vec2 size size / 16) sprite position

          bs = do
            (sprite, Point position) <- bullets
            pure $ tilesetAt 1.0 sprite position

          ss = Sparks.toSprites tilesetAt sparks
        in
          ss <> as <> bs

    debugTickE <- fromTimerDebug
    Network.reactimateDebugShow unlift $
      (debugTickE @> numBullets) <&> \num ->
        ('B', num)
