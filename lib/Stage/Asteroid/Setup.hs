{-# LANGUAGE OverloadedLists #-}

module Stage.Asteroid.Setup
  ( stackStage
  ) where

import RIO.Local

import DearImGui.Raw.IO qualified as ImGuiIO
import Engine.Camera qualified as Camera
import Engine.Types (StackStage(..), StageRIO)
import Engine.Types qualified as Engine
import Engine.Vulkan.Types (Queues)
import Engine.Worker qualified as Worker
import Foreign (nullPtr)
import Render.Basic qualified as Basic
import Render.DescSets.Set0 qualified as Set0
import Render.ImGui qualified as ImGui
import Resource.Buffer qualified as Buffer
import Resource.CommandBuffer (withPools)
import Resource.Region qualified as Region
import UnliftIO.Resource qualified as Resource
import Vulkan.Core10 qualified as Vk

import Common.Shroud qualified as Shroud
import Global.Render qualified
import Global.Resource.Assets qualified as Global
import Stage.Asteroid.Reactive qualified as Reactive
import Stage.Asteroid.Render qualified as Render
import Stage.Asteroid.Scene qualified as Scene
import Stage.Asteroid.Types (FrameResources(..), RunState(..), Stage)

-- loaderStage :: StackStage
-- loaderStage =
--   Loader.stackStage
--     (\_setMessage -> pure ())
--     (\() -> stackStage)

stackStage :: Global.Assets -> StackStage
stackStage globalAssets = StackStage $ stage globalAssets

stage :: Global.Assets -> Stage
stage globalAssets = Engine.Stage
  { sTitle = "Asteroid"

  , sAllocateRP = Basic.allocate_
  , sAllocateP  = Global.Render.allocatePipelines globalAssets
  , sInitialRS  = initialRunState globalAssets
  , sInitialRR  = initialFrameResources
  , sBeforeLoop = Region.exec setupLoop

  , sUpdateBuffers  = Render.updateBuffers
  , sRecordCommands = Render.recordCommands

  , sAfterLoop = Region.release
  }
  where
    setupLoop = do
      _eventNetwork <- Reactive.allocate

      ImGui.allocateLoop True
      ImGuiIO.setIniFilename nullPtr

initialRunState :: Global.Assets -> StageRIO env (Resource.ReleaseKey, RunState)
initialRunState rsGlobalAssets = Region.run $ withPools \pools -> do
  ortho <- Camera.spawnOrthoPixelsCentered

  -- rsViewP <- Region.local . Worker.registered $
  --   CameraControls.spawnViewOrbital Camera.initialOrbitalInput
  --     { Camera.orbitAzimuth = τ / 2
  --     , Camera.orbitAscent  = -τ / 4.01
  --     }

  -- rsCameraControls <- lift $ CameraControls.spawnControls rsViewP

  -- Region.local_ $ Worker.registerCollection rsCameraControls

  -- sceneInput <- Worker.newVar Scene.initialInput
  rsSceneP <- Scene.spawn ortho

  rsNebula <- Worker.newVar mempty

  rsMessages <- Worker.newVar mempty
  rsSprites <- Worker.newVar mempty

  rsDebugBox <- Worker.newVar (Left ())

  rsShroud <- Shroud.toBlack_ pools $ Just 1

  rsMaxBullets <- Worker.newVar 25

  let rsEvents = Nothing
  pure RunState{..}

initialFrameResources
  :: Queues Vk.CommandPool
  -> Basic.RenderPasses
  -> Global.Render.Pipelines
  -> ResourceT (Engine.StageRIO RunState) FrameResources
initialFrameResources _pools _passes pipelines = do
  Global.Assets{aCombined, aCubeMaps} <- gets rsGlobalAssets

  frScene <- Set0.allocate
    (Basic.getSceneLayout $ Global.Render.pBasic pipelines)
    (fmap snd aCombined)
    aCubeMaps
    Nothing -- lights
    []      -- shadows
    Nothing -- materials

  frNebula <- Buffer.newObserverCoherent "frNebula" Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT 1 mempty

  frMessages <- Buffer.newObserverCoherent "frMessages" Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT 128 mempty
  frSprites <- Buffer.newObserverCoherent "frSprites" Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT 2048 mempty

  frShroud <- gets rsShroud >>=
    Shroud.allocateResources

  pure FrameResources{..}
