{-# LANGUAGE RecursiveDo #-}

module Stage.Asteroid.Reactive.Bullets where

import RIO.Local

import Engine.Reactive.Collection qualified as Collection
import Engine.Reactive.Collider qualified as Collider
import Engine.Reactive.Fuse qualified as Fuse
import Engine.Reactive.Motion qualified as Motion
import Reactive.Banana (Behavior, Event)
import Reactive.Banana qualified as RB
import Reactive.Banana.Frameworks (MomentIO)

type Params = IVec2 -- XXX: sprite atlas pos

data New = New
  { params   :: Params
  , position :: RB.Moment (Behavior Point2)
  , fuse     :: Float
  }

data Active = Active
  { params   :: Params             -- ^ Some attributes
  , position :: Behavior Point2    -- ^ The current location of the bullet
  , expireE  :: Event Fuse.Expired -- ^ An event that fires whenever a bullet "expires" (bullets only have a finite lifetime)
  }

instance Collider.CollisionCircle (Motion.Located Params Point2) where
  getRadius = const 16
  getPosition = snd

setup
  :: Event Motion.ClockTick
  -> Event [New]
  -> Event IntSet
  -> MomentIO (Behavior [Motion.Located Params Point2])
setup tickE spawnE destroyE = mdo
  expireBulletsE <- Fuse.expire
    changeSetE
    (\Active{expireE} -> expireE)

  changeSetE <- Collection.setup
    spawnE
    (RB.unionWith mappend destroyE expireBulletsE)
    ( \New{params, fuse, position=positionM} -> do
        (expireE, _timer) <- Fuse.countdown tickE fuse
        position <- positionM
        pure Active{..}
    )

  Collection.collect
    changeSetE
    (\Active{position} -> position)
    (\Active{params} -> (params,))
