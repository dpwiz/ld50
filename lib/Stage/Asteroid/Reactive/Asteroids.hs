{-# LANGUAGE RecursiveDo #-}

module Stage.Asteroid.Reactive.Asteroids where

import RIO.Local

import Engine.Reactive.Collection qualified as Collection
import Engine.Reactive.Collider qualified as Collider
import Engine.Reactive.Motion qualified as Motion
import Reactive.Banana (Behavior, Event, (@>))
import Reactive.Banana qualified as RB
import Reactive.Banana.Frameworks qualified as RBF

import Common.Reactive qualified as Common

data Params = Params
  { size   :: Float
  , sprite :: IVec2
  }
  deriving (Eq, Ord, Show)

data New = New
  { params   :: Params
  , position :: RB.Moment (Behavior Point2)
  }

data Active = Active
  { params   :: Params
  , position :: Behavior Point2
  }

instance Collider.CollisionCircle (Motion.Located Params Point2) where
  getRadius (Params{size}, _pos) = size * 0.8 -- TODO: per-sprite

  getPosition = snd

setup
  :: Event [New]
  -> Event IntSet
  -> RBF.MomentIO
      ( Behavior [Motion.Located Params Point2]
      )
setup spawnE destroyE = mdo
  changeSetE <- Collection.setup
    spawnE
    destroyE
    ( \New{params, position=positionM} -> do
        position <- positionM
        pure Active{..}
    )

  Collection.collect
    changeSetE
    (\Active{position} -> position)
    (\Active{params} -> (params,))

new :: Event Motion.ClockTick -> Float -> New
new tickE seed = New
  { params = Params
    { sprite = sprite
    , size   = size * 16
    }
  , position =
      Motion.dirSpeed
        tickE
        (Point $ vec2 px py)
        (vec2 dx dy)
        speed
  }
  where
    sprite =
      Common.animFrame sprites 11 seed

    sprites =
      [ ivec2 3 1
      , ivec2 4 1
      , ivec2 5 1
      ]

    size =
      Common.animFrame [6, 4, 8, 10] 13 seed

    distAngle = sin seed * pi
    distance = 300 + cos seed * 50
    px = sin distAngle * distance
    py = cos distAngle * distance

    dirAngle = sin (seed * 2) * pi
    dx = sin dirAngle
    dy = cos dirAngle
    speed = sin seed * 8

autoSpawn
  :: Event Motion.ClockTick
  -> Behavior [Motion.Located Params Point2]
  -> Float
  -> RBF.MomentIO (Event [New])
autoSpawn tickE collection spawnRate = mdo
  spawnCountdown <- RB.accumB 0 $ RB.unions
    [ spawnE $> const 1.0
    -- , started $> const 0 -- XXX: spawn right away
    , (tickE @> collection) <&> \asteroids ->
        let
          current = length asteroids
        in
          if current == 0 then
            const 0
          else
            subtract $ spawnRate / fromIntegral current
    ]

  globalTime <- RBF.fromPoll getMonotonicTime
  let
    newAsteroids = do
      time <- globalTime
      pure
        [ new tickE (double2Float time * 10000)
        ]

    spawnE =
      RB.whenE (fmap (<= 0) spawnCountdown) tickE

  pure (spawnE @> newAsteroids)
