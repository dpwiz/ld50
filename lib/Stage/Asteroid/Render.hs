{-# LANGUAGE OverloadedLists #-}

module Stage.Asteroid.Render
  ( updateBuffers
  , recordCommands
  ) where

import RIO.Local

import Engine.Types qualified as Engine
import Engine.Vulkan.Pipeline.Graphics qualified as Graphics
import Engine.Vulkan.Swapchain qualified as Swapchain
import Engine.Worker qualified as Worker
import Render.Basic qualified as Basic
import Render.DescSets.Set0 qualified as Set0
import Render.Draw qualified as Draw
import Render.ForwardMsaa qualified as ForwardMsaa
-- import Render.ImGui qualified as ImGui
import Resource.Buffer qualified as Buffer
import Vulkan.Core10 qualified as Vk

import Common.Shroud qualified as Shroud
import Global.Render (Pipelines(..), StageFrameRIO)
-- import Stage.Asteroid.Render.Imgui (imguiDrawData)
import Stage.Asteroid.Types (FrameResources(..), RunState(..))

updateBuffers
  :: RunState
  -> FrameResources
  -> StageFrameRIO FrameResources RunState ()
updateBuffers RunState{..} FrameResources{..} = do
  Set0.observe rsSceneP frScene
  Buffer.observeCoherentResize_ rsNebula frNebula
  Buffer.observeCoherentResize_ rsSprites frSprites
  Buffer.observeCoherentResize_ rsMessages frMessages

recordCommands
  :: Vk.CommandBuffer
  -> FrameResources
  -> Word32
  -> StageFrameRIO FrameResources RunState ()
recordCommands cb _fr@FrameResources{..} imageIndex = do
  (_context, Engine.Frame{fSwapchainResources, fRenderpass, fPipelines}) <- ask
  let Pipelines{..} = fPipelines

  -- dear <- imguiDrawData

  ForwardMsaa.usePass (Basic.rpForwardMsaa fRenderpass) imageIndex cb do
    Swapchain.setDynamicFullscreen cb fSwapchainResources

    Set0.withBoundSet0 frScene (Basic.pWireframe pBasic) cb do
      Graphics.bind cb (Basic.pSprite pBasic) do
        Worker.readObservedIO frNebula >>=
          Draw.quads cb

        Worker.readObservedIO frSprites >>=
          Draw.quads cb

      Graphics.bind cb (Basic.pEvanwSdf pBasic) $
        Worker.readObservedIO frMessages >>=
          Draw.quads cb

    -- ImGui.draw dear cb
    -- XXX: copypasta

    static <- gets rsShroud
    scene <- gets rsSceneP >>= Worker.getOutputData
    Set0.withBoundSet0 frScene (Basic.pUnlitColoredNoDepth pBasic) cb $
      Graphics.bind cb (Basic.pUnlitColoredNoDepth pBasic) do
        Shroud.draw cb scene static frShroud
