module Stage.Asteroid.Types
  ( Stage
  , FrameResources(..)
  , RunState(..)
  ) where

import RIO.Local

-- import Engine.Camera qualified as Camera
-- import Engine.Camera.Controls qualified as CameraControls
-- import Reactive.Banana.Frameworks (EventNetwork)
import Engine.Events qualified as Events
import Engine.UI.Layout qualified as Layout
import Engine.Worker qualified as Worker
import Render.DescSets.Set0 qualified as Set0
import Render.Font.EvanwSdf.Model qualified as EvanwSdf
import Render.Unlit.Sprite.Model qualified as Sprite
import Resource.Buffer qualified as Buffer
import RIO.Vector.Storable qualified as Storable

import Common.Shroud qualified as Shroud
import Global.Render qualified
import Global.Resource.Assets qualified as Global

type Stage = Global.Render.Stage FrameResources RunState

data FrameResources = FrameResources
  { frScene :: Set0.FrameResource '[Set0.Scene]

  , frNebula :: Buffer.ObserverCoherent Sprite.InstanceAttrs

  , frMessages :: Buffer.ObserverCoherent EvanwSdf.InstanceAttrs
  , frSprites :: Buffer.ObserverCoherent Sprite.InstanceAttrs

  , frShroud :: Shroud.Resources
  }

data RunState = RunState
  { rsEvents :: Maybe (Events.Sink () RunState)
  , rsSceneP :: Set0.Process
  , rsGlobalAssets :: Global.Assets

  , rsNebula :: Worker.Var (Storable.Vector Sprite.InstanceAttrs)

  , rsMessages :: Worker.Var (Storable.Vector EvanwSdf.InstanceAttrs)
  , rsSprites :: Worker.Var (Storable.Vector Sprite.InstanceAttrs)

  , rsDebugBox :: Worker.Var (Either ({- collapse -}) (Layout.Box, Text))

  , rsShroud :: Shroud.State

  , rsMaxBullets :: Worker.Var Int
  }
