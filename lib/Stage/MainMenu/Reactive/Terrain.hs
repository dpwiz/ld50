module Stage.MainMenu.Reactive.Terrain
  ( Env
  , Update
  , Snapshot
  , setup

  , tileSize
  , tileScale

  , readChunkTile
  , readChunkSolid
  , readChunkSolidNeighbors

  , TilePos(..)
  , toTilePos
  ) where

import RIO.Local

import Data.Bits (complement)
import Geometry.Tile.Neighbors (Neighbors)
import Geometry.Tile.Neighbors qualified as Neighbors
import Geometry.Tile.Microblob (Microblob(..))
import Geometry.Tile.Microblob qualified as Microblob
import Data.STRef (STRef, newSTRef, readSTRef, writeSTRef)
import Data.These (These(..))
import Data.Vector.Storable qualified as Storable
import Data.Vector.Storable.Mutable qualified as Storable (write)
import Engine.Window.MouseButton qualified as MouseButton
import Geometry.Quad (Quad(..))
import Reactive.Banana qualified as RB
import Render.Unlit.Sprite.Model qualified as Sprite
import RIO.List qualified as List
import RIO.Map qualified as Map
import System.Random.MWC.Distributions qualified as Distributions
import System.Random.Stateful qualified as Random
import Vulkan.Zero (zero)

import Engine.ReactiveBanana.Stateful qualified as Stateful

type Env =
  ( Vec2 -> IVec2 -> Vec2 -> Sprite.InstanceAttrs
  )

type Trigger =
  These
    Double
    (MouseButton.ModifierKeys, TilePos)

type Update =
  ( Maybe (Storable.Vector Sprite.InstanceAttrs)
  , Maybe ()
  )

type Snapshot =
  ( Random.StdGen
  , Map ChunkPos Chunk
  , Map ChunkPos Chunk
  )

type Vars s =
  ( Random.STGenM Random.StdGen s
  , STRef s ChunkMap
  , STRef s ChunkMap
  )

type instance Stateful.Thaw Snapshot s = Vars s

setup
  :: (MonadIO m, RB.MonadMoment m)
  => Env
  -> RB.Event Trigger
  -> m (RB.Event Update, RB.Behavior Snapshot)
setup env =
  Stateful.setup initialWorld \event snapshot ->
    runST $
      Stateful.runWorldWith thawWorld freezeWorld snapshot \st ->
        dispatch env st event
  where
    initialWorld = (,,)
      <$> Random.newStdGen
      <*> pure Map.empty
      <*> pure Map.empty

    thawWorld (gen, bg, fg) =
      (,,)
        <$> Random.newSTGenM gen
        <*> newSTRef bg
        <*> newSTRef fg

    freezeWorld (gen, bg, fg) =
      (,,)
        <$> readSTRef (Random.unSTGenM gen)
        <*> readSTRef bg
        <*> readSTRef fg

dispatch
  :: Env
  -> Vars s
  -> Trigger
  -> ST s Update
dispatch env st = \case
  This _slowTick ->
    updateWorldTick env st
  That (_mods, tilePos) ->
    updateWorldClick env st tilePos
  These _slowTick (_mods, tilePos) -> do
    _ <- updateWorldClick env st tilePos
    updateWorldTick env st

updateWorldTick :: Env -> Vars s -> ST s Update
updateWorldTick env st@(gen, _bgChunks, _fgChunks) = do
  clickRandom <- do
    angle    <- (* τ) <$> Random.uniformFloat01M gen
    distance <- double2Float <$> Distributions.normal 0 512 gen
    pure . toTilePos $
      vec2
        (cos angle * distance)
        (sin angle * distance)

  updateWorldClick env st clickRandom
  -- _ <- updateWorldClick clickRandom
  -- _ <- updateWorldClick $ clickRandom - vec2 0 tileSize
  -- _ <- updateWorldClick $ clickRandom + vec2 0 tileSize
  -- _ <- updateWorldClick $ clickRandom + vec2 tileSize 0
  -- updateWorldClick $ clickRandom - vec2 tileSize 0

updateWorldClick :: Env -> Vars s -> TilePos -> ST s Update
updateWorldClick tilesetAt (_gen, bgChunks, fgChunks) tilePos = do
  bgDirty <- newSTRef False

  let
    newTile =
      withVec2 (unTilePos tilePos) \tx ty ->
        if odd @Int $ truncate (tx + ty) then
          ivec2 2 0
        else
          ivec2 3 0

  placeBgTile tilesetAt bgDirty bgChunks tilePos newTile

  isDirty <- readSTRef bgDirty
  if isDirty then do
    newBgChunks <- readSTRef bgChunks
    _newFgChunks <- readSTRef fgChunks
    let
      worldUpdate =
        Storable.concat
          (blendAttrs : attrChunks)

      (attrChunks, blendChunks) = List.unzip do
        Chunk{attrs, blends} <- Map.elems newBgChunks
        pure (attrs, blends)

      blendAttrs =
        Storable.fromList $
          concat $ concatMap Map.elems blendChunks

    pure (Just worldUpdate, Nothing)
  else
    pure (Nothing, Nothing)

newtype TilePos = TilePos { unTilePos :: Vec2 }
  deriving (Eq, Ord, Show, Num)

toTilePos :: Vec2 -> TilePos
toTilePos cursorPos =
  TilePos $
    emap (fromInteger . round . (/ tileSize)) cursorPos

newtype ChunkPos = ChunkPos Vec2
  deriving (Eq, Ord, Show)

toChunkPos :: TilePos -> ChunkPos
toChunkPos (TilePos tilePos) =
  ChunkPos $
    emap (fromInteger . round . (/ chunkSizeTiles)) tilePos

newtype TilePosLocal = TilePosLocal Vec2

tilePosLocal :: ChunkPos -> TilePos -> TilePosLocal
tilePosLocal (ChunkPos chunkPos) (TilePos tilePos) =
  TilePosLocal $
    tilePos - chunkPos ^* chunkSizeTiles

tileChunkIndex :: TilePosLocal -> Int
tileChunkIndex (TilePosLocal localPos) =
  withVec2 localPos \lx ly ->
    let
      column = truncate lx + (numColumns - 1) `div` 2
      row    = truncate ly + (numColumns - 1) `div` 2

      numColumns = truncate chunkSizeTiles
    in
      row * numColumns + column

tileSize :: Float
tileSize = 32

tileScale :: Vec2
tileScale = 2

blendSize :: Float
blendSize = tileSize / 2

chunkSizeTiles :: Float
chunkSizeTiles = 15

numChunkTiles :: Int
numChunkTiles = truncate $ chunkSizeTiles * chunkSizeTiles

type ChunkMap = Map ChunkPos Chunk

data Chunk = Chunk
  { tiles  :: Storable.Vector IVec2
  , blends :: Map TilePos [Sprite.InstanceAttrs]
  , solids :: Storable.Vector Word8 -- TODO: compress to bitset?
  , adjas  :: Storable.Vector Word8
  , attrs  :: Storable.Vector Sprite.InstanceAttrs
  }

readChunkTile
  :: TilePos
  -> ChunkMap
  -> Maybe IVec2
readChunkTile tilePos chunkMap = do
  Chunk{tiles} <- Map.lookup chunkPos chunkMap
  case Storable.unsafeIndex tiles ix of
    -1 -> Nothing
    ok -> Just ok
  where
    chunkPos = toChunkPos tilePos
    ix = tileChunkIndex (tilePosLocal chunkPos tilePos)

readChunkSolid
  :: ChunkMap
  -> TilePos
  -> Bool
readChunkSolid chunkMap tilePos =
  case Map.lookup chunkPos chunkMap of
    Just Chunk{solids} ->
      Storable.unsafeIndex solids ix == 1
    Nothing ->
      False
  where
    chunkPos = toChunkPos tilePos
    ix = tileChunkIndex (tilePosLocal chunkPos tilePos)

readChunkSolidNeighbors
  :: ChunkMap
  -> TilePos
  -> Neighbors Bool
readChunkSolidNeighbors chunkMap tilePos =
  case Map.lookup chunkPos chunkMap of
    Just Chunk{adjas} ->
      Neighbors.fromBitsNW $
        Storable.unsafeIndex adjas ix
    Nothing ->
      pure False
  where
    chunkPos = toChunkPos tilePos
    ix = tileChunkIndex (tilePosLocal chunkPos tilePos)

alterChunk
  :: (Chunk -> Chunk)
  -> ChunkPos
  -> ChunkMap
  -> ChunkMap
alterChunk f =
  Map.alter (Just . f . fromMaybe emptyChunk)
  where
    emptyChunk = Chunk
      { tiles  = Storable.replicate numChunkTiles (-1)
      , blends = Map.empty
      , solids = Storable.replicate numChunkTiles 0
      , adjas  = Storable.replicate numChunkTiles 0
      , attrs  = Storable.replicate numChunkTiles zero
      }

writeChunkTile
  :: (IVec2, Word8, Sprite.InstanceAttrs)
  -> TilePos
  -> ChunkMap
  -> ChunkMap
writeChunkTile (tile, adjacencyIndex, tileSprite) tilePos =
  alterChunk writeNewTile chunkPos
  where
    chunkPos = toChunkPos tilePos

    ix =
     tileChunkIndex (tilePosLocal chunkPos tilePos)

    writeNewTile Chunk{..} = Chunk
      { tiles =
          Storable.modify
            (\mvec -> Storable.write mvec ix tile)
            tiles

      , blends =
          Map.delete tilePos blends

      , solids =
          Storable.modify
            (\mvec -> Storable.write mvec ix 1)
            solids

      , adjas =
          Storable.modify
            (\mvec -> Storable.write mvec ix adjacencyIndex)
            adjas

      , attrs =
          Storable.modify
            (\mvec -> Storable.write mvec ix tileSprite)
            attrs
      }

writeChunkAdjacency
  :: Word8
  -> TilePos
  -> ChunkMap
  -> ChunkMap
writeChunkAdjacency adjacencyIndex tilePos =
  alterChunk writeBlends chunkPos
  where
    writeBlends chunk = chunk
      { adjas = Storable.modify
          (\mvec -> Storable.write mvec ix adjacencyIndex)
          (adjas chunk)
      }

    ix =
     tileChunkIndex (tilePosLocal chunkPos tilePos)

    chunkPos =
      toChunkPos tilePos

writeChunkBlends
  :: (Word8, [Sprite.InstanceAttrs])
  -> TilePos
  -> ChunkMap
  -> ChunkMap
writeChunkBlends (adjacencyIndex, tileBlends) tilePos =
  alterChunk writeBlends chunkPos
  where
    writeBlends chunk = chunk
      { blends = Map.insert tilePos tileBlends (blends chunk)
      , adjas = Storable.modify
          (\mvec -> Storable.write mvec ix adjacencyIndex)
          (adjas chunk)
      }

    ix =
     tileChunkIndex (tilePosLocal chunkPos tilePos)

    chunkPos =
      toChunkPos tilePos

placeBgTile
  :: (Vec2 -> IVec2 -> Vec2 -> Sprite.InstanceAttrs)
  -> STRef s Bool
  -> STRef s ChunkMap
  -> TilePos
  -> IVec2
  -> ST s ()
placeBgTile tilesetAt bgDirty bgChunks tilePos newTile = do
  oldBgSnapshot <- readSTRef bgChunks
  let
    sameTile =
      readChunkTile tilePos oldBgSnapshot == Just newTile

  unless sameTile do
    let
      adjacencyIndex =
        fromIntegral . Neighbors.toBitsNW $
          fmap (readChunkSolid oldBgSnapshot) neighborPos

      terrainTile (TilePos tp) =
        tilesetAt tileScale newTile (tp ^* tileSize)

      updatedBg =
        writeChunkTile
          (newTile, adjacencyIndex, terrainTile tilePos)
          tilePos
          oldBgSnapshot

      blendedBg =
        updateBlends tilesetAt neighborPos updatedBg

    writeSTRef bgChunks blendedBg
    writeSTRef bgDirty True
  where
    neighborPos = fmap (+ tilePos) neighborTileDir

updateBlends
  :: (Vec2 -> IVec2 -> Vec2 -> Sprite.InstanceAttrs)
  -> Neighbors TilePos
  -> ChunkMap
  -> ChunkMap
updateBlends tilesetAt neighbors chunks = foldr f chunks neighbors
  where
    f tilePos acc =
      if solid tilePos then
        writeChunkAdjacency
          (fromIntegral adjacencyIndex)
          tilePos
          acc
      else
        writeChunkBlends
          (fromIntegral adjacencyIndex, newBlends)
          tilePos
          acc
      where
        adjacencyIndex =
          Neighbors.toBitsNW $
            neighborTileDir <&> \dir ->
              solid (dir + tilePos)

        blends =
          Microblob.quad
            Neighbors.bitsNW
            microblobBlends
            (complement adjacencyIndex)

        newBlends =
          catMaybes . toList $
            renderSubs (tilesetAt blendScale) tilePos <*> blends

        blendScale = 1 -- XXX: global

    solid = readChunkSolid chunks

renderSubs
  :: (IVec2 -> Vec2 -> Sprite.InstanceAttrs)
  -> TilePos
  -> Quad (Maybe IVec2 -> Maybe Sprite.InstanceAttrs)
renderSubs tilesetAt (TilePos tp) = Quad
  { quadLT = fmap \uv -> tilesetAt uv (subPos $ vec2 (-0.5) (-0.5))
  , quadRT = fmap \uv -> tilesetAt uv (subPos $ vec2   0.5  (-0.5))
  , quadLB = fmap \uv -> tilesetAt uv (subPos $ vec2 (-0.5)   0.5)
  , quadRB = fmap \uv -> tilesetAt uv (subPos $ vec2   0.5    0.5)
  }
  where
    subPos subDir =
      tp ^* tileSize + subDir ^* blendSize

microblobBlends :: Microblob (Maybe IVec2)
microblobBlends = Microblob
  { brCornerInner    = Just $ ivec2 0 28
  , blCornerInner    = Just $ ivec2 2 28
  , trCornerInner    = Just $ ivec2 0 30
  , tlCornerInner    = Just $ ivec2 2 30
  , tlCornerOuter    = Just $ ivec2 0 25
  , ttEdgeHorizontal = Just $ ivec2 1 25
  , trCornerOuter    = Just $ ivec2 2 25
  , llEdgeVertical   = Just $ ivec2 0 26
  , full             = Just $ ivec2 1 26
  , rrEdgeVertical   = Just $ ivec2 2 26
  , blCornerOuter    = Just $ ivec2 0 27
  , bbEdgeHorizontal = Just $ ivec2 1 27
  , brCornerOuter    = Just $ ivec2 2 27
  }

neighborTileDir :: Neighbors.Neighbors TilePos
neighborTileDir = Neighbors.directionsWith \dx dy -> TilePos (vec2 dx dy)
