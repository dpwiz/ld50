module Stage.MainMenu.Reactive.Utils where

import RIO.Local

import Engine.UI.Layout qualified as Layout
import Reactive.Banana qualified as RB

cursorHovering :: RB.Behavior Vec2 -> RB.Behavior (Maybe TRBL) -> RB.Behavior Bool
cursorHovering cursorPos zone = do
  pos <- cursorPos
  trbl <- zone
  pure case trbl of
    Nothing ->
      False
    Just trbl' ->
      pointInTRBL trbl' pos

data TRBL = TRBL
  {-# UNPACK #-}!Float
  {-# UNPACK #-}!Float
  {-# UNPACK #-}!Float
  {-# UNPACK #-}!Float
  deriving (Eq, Show)

instance Semigroup TRBL where
  {-# INLINE (<>) #-}
  TRBL t1 r1 b1 l1 <> TRBL t2 r2 b2 l2 =
    TRBL
      (min t1 t2)
      (max r1 r2)
      (max b1 b2)
      (min l1 l2)

{-# INLINE fromXYWH #-}
fromXYWH :: Vec4 -> TRBL
fromXYWH xywh = withVec4 xywh \x y w h ->
  let
    t = y - hh
    r = x + hw
    b = y + hh
    l = x - hw

    hw = w / 2
    hh = h / 2
  in
    TRBL t r b l

{-# INLINE _toBox #-}
_toBox :: TRBL -> Layout.Box
_toBox (TRBL t r b l) = Layout.Box
  { boxPosition = vec2 (r/2 + l/2) (b/2 + t/2)
  , boxSize     = vec2 (r - l) (b - t)
  }

{-# INLINE pointInTRBL #-}
pointInTRBL :: TRBL -> Vec2 -> Bool
pointInTRBL (TRBL t r b l) pos =
  withVec2 pos \x y ->
    y >= t &&
    x <= r &&
    y <= b &&
    x >= l
