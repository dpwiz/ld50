{-# LANGUAGE RecursiveDo #-}

module Stage.MainMenu.Reactive.Menu
  ( setup
  ) where

import RIO.Local

import Data.List.NonEmpty (nonEmpty)
import Data.Semigroup (sconcat)
import Data.Vector.Storable qualified as Storable
import Engine.UI.Layout qualified as Layout
import Engine.UI.Layout.Linear qualified as Linear
import Engine.UI.Message qualified as Message
import Engine.Window.MouseButton qualified as MouseButton
import Engine.ReactiveBanana (debounce)
import Formatting qualified as F
import Geomancy.Interpolate qualified as I
import Reactive.Banana ((@>))
import Reactive.Banana qualified as RB
import Reactive.Banana.Frameworks qualified as RBF
import Render.Font.EvanwSdf.Model qualified as EvanwSdf

import Common.Reactive (dtFast)
import Global.Resource.Assets qualified as Global
import Global.Resource.Font (Font)
import Global.Resource.Font qualified as Font
import Stage.MainMenu.Reactive.Utils qualified as Utils

type ClickableText =
  ( RB.Event (Storable.Vector EvanwSdf.InstanceAttrs)
  , RB.Event ()
  )

setup
  :: Global.Assets
  -> RB.Behavior Layout.Box
  -> RB.Event ()
  -> RB.Event Double
  -> (RB.Event Vec2, RB.Behavior Vec2)
  -> MouseButton.Collection (RB.Event a4)
  -> RBF.MomentIO
      ( RB.Behavior Bool
      , ClickableText
      , ClickableText
      , RB.Event (Storable.Vector EvanwSdf.InstanceAttrs)
      )
setup Global.Assets{aFonts} parent actuated fastTickE cursor mouseClicked = mdo
  let
    messageSmall = messageBase (Font.small aFonts) 24
    messageLarge = messageBase (Font.large aFonts) 64

    makeClickable = activeItem (fst cursor) mouseClicked actuated

  let
    menuBox =
      parent <&> \box ->
        case Linear.hBoxShares @[] [1, 1, 1] box of
          [_leftPad, column, _rightPad] ->
            case Linear.vBoxShares @[] [1, 1, 1, 3 ] column of
              [_topPad, startBox, dontStartBox, _bottomPad] ->
                (startBox, dontStartBox)
              _ ->
                error "same number"
          _ ->
            error "same number"

  --------------

  start <- hoverableMessage
    cursor
    (fmap fst menuBox)
    (\msg -> msg { Message.inputColor = vec4 0.5 1 0.25 1 })
    messageLarge
      { Message.inputText = "Start"
      , Message.inputColor = vec4 1 0.5 0.25 1
      , Message.inputOrigin = Layout.CenterBottom
      }

  startClickable <- makeClickable start

  --------------

  dontStart <- hoverableMessage
    cursor
    (fmap snd menuBox)
    (\msg -> msg { Message.inputColor = vec4 0.5 1 0.25 1 })
    messageLarge
      { Message.inputText = "Don't start"
      , Message.inputColor = vec4 1 0.5 0.25 1
      , Message.inputOrigin = Layout.CenterTop
      }

  dontStartClickable <- makeClickable dontStart

  --------------

  timeAttrsE <- setupTimeTicker fastTickE (snd dontStartClickable) parent messageSmall

  --------------

  let
    menuHovering =
      fmap or $ traverse @[] fst
        [ dontStart
        , start
        ]

  pure
    ( menuHovering
    , startClickable
    , dontStartClickable
    , timeAttrsE
    )

setupTimeTicker
  :: RB.Event a1
  -> RB.Event a2
  -> RB.Behavior Layout.Box
  -> Message.Input
  -> RBF.MomentIO (RB.Event (Storable.Vector EvanwSdf.InstanceAttrs))
setupTimeTicker fastTickE triggerE parent messageSmall = mdo
  let price = 15.0 :: Float

  timeTillStart <- RB.accumB price $ RB.unions
    [ fastTickE $> subtract dtFast
    , eSold $> const price
    , triggerE $> const price
    ]

  let eSold = RB.whenE (fmap (<= 0) timeTillStart) fastTickE

  pure $
    fastTickE @> do
      time <- fmap (max 0) timeTillStart
      parentBox <- parent
      pure $
        Message.mkAttrs parentBox messageSmall
          { Message.inputText =
              F.sformat (F.fixed 1) time
          , Message.inputColor =
              I.cubic
                0
                (vec4 0.25 0.5 2 1)
                (vec4 0.25 2 0.5 1)
                0
                (1 - min price time / price)
          , Message.inputOrigin =
              Layout.CenterTop
          }

activeItem
  :: RB.Event cursorPos
  -> MouseButton.Collection (RB.Event click)
  -> RB.Event ()
  -> (RB.Behavior Bool, RB.Behavior attrs)
  -> RBF.MomentIO (RB.Event attrs, RB.Event ())
activeItem cursorPosE mouseClicked started (hover, attrs) = do
  hoverChanged <- debounce False $ cursorPosE @> hover
  pure
    ( RB.merge started hoverChanged @> attrs
    , RB.whenE hover (MouseButton.mb1 mouseClicked $> ())
    )

hoverableMessage
  :: (RB.Event a, RB.Behavior Vec2)
  -> RB.Behavior Layout.Box
  -> (Message.Input -> Message.Input)
  -> Message.Input
  -> RBF.MomentIO
      ( RB.Behavior Bool
      , RB.Behavior (Storable.Vector EvanwSdf.InstanceAttrs)
      )
hoverableMessage (cursorPosE, cursorPos) parent hoverF messageInput = mdo
  let
    attrsIdle =
      messageB parent messageInput

    attrsHover =
      messageB parent (hoverF messageInput)

  messageAttrs <- RB.switchB attrsIdle $
    cursorPosE @>
      titleMessageHover <&>
        bool attrsIdle attrsHover

  let
    titleMessageHover = Utils.cursorHovering cursorPos titleMessageZone
      where
        titleMessageZone = messageAttrs <&> \attrs ->
          fmap sconcat $ nonEmpty do
            EvanwSdf.InstanceAttrs{vertRect} <- Storable.toList attrs
            pure $
              Utils.fromXYWH $ vertRect * vec4 1 1 1 (-1) -- Fix negative height in PutChar.pcSize

  pure
    ( titleMessageHover
    , messageAttrs -- updateAttrsE
    )

messageB :: RB.Behavior Layout.Box -> Message.Input -> RB.Behavior (Storable.Vector EvanwSdf.InstanceAttrs)
messageB parentB input =
  parentB <&> \parent ->
    Message.mkAttrs parent input

messageBase :: Font -> Float -> Message.Input
messageBase (fontId, font) size = Message.Input
  { inputText         = ""
  , inputOrigin       = Layout.Center
  , inputSize         = size
  , inputColor        = 1
  , inputFont         = font
  , inputFontId       = fontId
  , inputOutline      = vec4 0 0 0 1
  , inputOutlineWidth = 4/16
  , inputSmoothing    = 1/16
  }
