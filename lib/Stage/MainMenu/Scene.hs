module Stage.MainMenu.Scene where

import RIO.Local

import Engine.Camera qualified as Camera
import Engine.Worker qualified as Worker
import Render.DescSets.Set0 (Scene(..), emptyScene)
-- import Geomancy.Transform qualified as Transform

-- type InputVar = Worker.Var Input

-- type Input = Camera.ProjectionInput 'Camera.Orthographic

-- initialInput :: Input
-- initialInput = Input

type Process = Worker.Merge Scene

spawn
  :: ( Worker.HasOutput proj
     , Worker.GetOutput proj ~ Camera.Projection 'Camera.Orthographic
     , MonadUnliftIO m
     )
  => proj
  -> ResourceT m Process
spawn = Worker.spawnMerge1 mkScene

mkScene
  :: Camera.Projection 'Camera.Orthographic
  -- -> Camera.View
  -- -> Input
  -> Scene
mkScene Camera.Projection{..} = -- Camera.View{..} Camera.ProjectionInput{..} =
  emptyScene
    { sceneProjection    = projectionTransform
    , sceneInvProjection = projectionInverse
    }
