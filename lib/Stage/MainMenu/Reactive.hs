{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE RecursiveDo #-}

module Stage.MainMenu.Reactive
  ( allocate
  ) where

import RIO.Local

import Data.These (These(..))
import Data.Vector.Storable qualified as Storable
import Engine.Reactive.Motion qualified as Motion
import Engine.ReactiveBanana qualified as Network
import Engine.ReactiveBanana.Course qualified as Course
import Engine.ReactiveBanana.Window qualified as Window
import Engine.StageSwitch (trySwitchStage)
import Engine.Types (StageRIO)
import Engine.Types qualified as Engine
import Engine.UI.Layout qualified as Layout
import Engine.Window.CursorPos qualified as CursorPos
import Engine.Window.MouseButton qualified as MouseButton
import Reactive.Banana ((<@>), (@>))
import Reactive.Banana qualified as RB
import Reactive.Banana.Frameworks qualified as RBF
import Render.Unlit.Sprite.Model qualified as Sprite
import Resource.Image.Atlas qualified as Atlas
import RIO.State (get)

import Common.Reactive (dtFast)
import Common.Reactive qualified as Common
import Common.Reactive.Sparks qualified as Sparks
import Global.Resource.Assets qualified as Global
import Global.Resource.Texture qualified as Texture
import Stage.Asteroid.Setup qualified as Asteroid (stackStage)
import Stage.MainMenu.Reactive.Menu qualified as Menu
import Stage.MainMenu.Reactive.Terrain qualified as Terrain
import Stage.MainMenu.Types (RunState(..))

allocate :: ResourceT (StageRIO RunState) RBF.EventNetwork
allocate = do
  fromCursorPos <- Network.eventHandler $ CursorPos.callback . curry
  fromMouseButton <- Network.eventHandler MouseButton.callback
  fromTimerFast <- Network.timer $ round (1e6 * dtFast)
  fromTimerSlow <- Network.timer 1e6
  mkTimerSparks <- Motion.spawnTimer dtFast

  RunState{..} <- get
  let Global.Assets{aTextures} = rsGlobalAssets

  tilesetAtlas <-
    case Atlas.fromImageSize (uvec2 112 832) 16 0 of
      Left _overs ->
        throwString "Invalid tile atlas size"
      Right ok ->
        pure ok
  let
    tilesetAt scale (WithIVec2 atlasX atlasY) =
      Sprite.fromAtlas
        (Texture.medieval aTextures)
        tilesetAtlas
        scale
        (vec2 (fromIntegral atlasX) (fromIntegral atlasY))

  Network.allocateActuated \(UnliftIO unlift) started -> mdo
    fastTickE <- fromTimerFast
    globalTime <- RBF.fromPoll getMonotonicTime

    -------------------

    -- XXX: screen-sized layout base
    screenBox <- Window.setupScreenBox (liftIO . unlift)

    -- XXX: some derived box
    let
      screenBoxPadded =
        screenBox <&> \box ->
          Layout.boxPadAbs box 64

    -------------------

    -- XXX: project window cursor position to layout
    (cursorPosE, cursorPos) <- Window.setupCursorPos fromCursorPos screenBox

    -------------------

    mouseClicked' <- Window.setupMouseClicks fromMouseButton cursorPos
    -- for_ (zip (toList MouseButton.collectionGlfw) (toList mouseClicked)) \(btn, clickE) ->
    --   reactimateDebugShow $ fmap (btn,) clickE

    let
      mouseClicked =
        fmap (Course.whenIdle gameStarting) mouseClicked'

    -------------------

    (menuStartingE, menuStartedE, _menuStarting) <-
      Course.setup (started $> 1.0) $
        fastTickE $> \old ->
          if old <= dtFast then
            Left Nothing
          else
            Right (old - dtFast * 2)
    Network.pushWorkerInput rsShroud $ fmap Just menuStartingE
    Network.pushWorkerInput rsShroud menuStartedE

    -- XXX: all clicks are pre-filtered out fade-out
    (gameStartingE, gameStartedE, gameStarting) <-
      Course.setup (snd startMessage $> 1.0) $
        fastTickE $> \old ->
            if old <= dtFast then
              Left ()
            else
              Right (old - dtFast)
    Network.pushWorkerInput rsShroud $ fmap (Just . (1.0 -)) gameStartingE
    RBF.reactimate $ gameStartedE $>
      unlift do
        changed <- trySwitchStage . Engine.PushRestart $
          Asteroid.stackStage rsGlobalAssets
        logDebug $ "trySwitchStage: " <> displayShow changed

    -------------------

    menu <- Menu.setup
      rsGlobalAssets
      screenBoxPadded
      started
      (Course.whenIdle gameStarting fastTickE)
      (cursorPosE, cursorPos)
      mouseClicked

    let (menuHovering, startMessage, dontStartMessage, timeAttrsE) = menu
    Network.pushWorkerOutput rsStartMessage (fst startMessage)
    Network.pushWorkerOutput rsDontStartMessage (fst dontStartMessage)
    Network.pushWorkerOutput rsTimeMessage timeAttrsE

    -------------------

    Network.pushWorkerOutput rsNebula $
      fastTickE <&> Common.animateNebula aTextures

    -------------------

    tileCursorE <- mkTileCursor fastTickE globalTime cursorPosE (cursorSprite tilesetAt)
    Network.pushWorkerOutput rsTileCursor $
      fmap fst tileCursorE

    -------------------

    let
      tileClickedE =
        RB.whenE (fmap not menuHovering) $
          fmap (second Terrain.toTilePos) (MouseButton.mb1 mouseClicked)
    Network.reactimateDebugShow unlift tileClickedE

    (terrainE, terrain) <- Terrain.setup tilesetAt $
      RB.merge RB.never tileClickedE
      -- RB.merge slowTickE tileClickedE
      -- RB.merge fastTickE tileClickedE

    Network.pushWorkerOutputJust rsTileMap (fmap fst terrainE)

    -------------------

    slowTickE <- fromTimerSlow

    (spritesSlowE, _b) <-
      RB.mapAccum [] $
        dispatchSprites tilesetAt <$> terrain <@> RB.merge slowTickE tileClickedE

    Network.pushWorkerOutput rsSpritesSlow spritesSlowE

    -------------------

    sparkTickE <- mkTimerSparks

    sparkPositions <- Sparks.setup
      sparkTickE
      (spawnSparks <$> globalTime <@> fmap snd tileClickedE)

    let
      spawnSparks seed (Terrain.TilePos tilePos) =
        Sparks.explode sparkTickE seed (Point $ tilePos ^* Terrain.tileSize)

    let
      spritesFastE = fastTickE @> do
        sparks <- sparkPositions
        pure $
          Storable.fromList $
            Sparks.toSprites tilesetAt sparks

    Network.pushWorkerOutput rsSpritesFast spritesFastE

    -------------------

    let
      debugValE = fmap snd terrainE
      debugBoxE = Common.mkDebugBox <$> screenBox <*> screenBoxPadded <@> debugValE
    Network.pushWorkerOutputJust rsDebugBox debugBoxE

type MySprite =
  ( Integer
  , Integer
  , Vector IVec2
  , Terrain.TilePos
  )

dispatchSprites
  :: (Vec2 -> IVec2 -> Vec2 -> Sprite.InstanceAttrs)
  -> Terrain.Snapshot
  -> These time (mods, Terrain.TilePos)
  -> [MySprite]
  -> (Sprite.StorableAttrs, [MySprite])
dispatchSprites tilesetAt terrain event old =
  case event of
    These time click ->
      dispatchSprites tilesetAt terrain (This time) old &
      dispatchSprites tilesetAt terrain (That click) . snd

    This _time ->
      let
        new = do
          ((+ 1) -> tick, life, frames, pos) <- old
          when (life > 0) $
            guard (tick < life)
          pure (tick, life, frames, pos)
      in
        ( render new
        , new
        )

    That (_mods, tilePos) ->
      ( render new
      , new
      )
      where
        new = withLand $ withMagic do
          piece@(_, _, _, oldPos) <- old
          guard $ oldPos /= tilePos
          pure piece

        withMagic = (:)
          if not terrainSolid then
            sprites 3 animatedSpiral
          else
            sprites 3 animatedSparkles

        withLand
          | not terrainSolid = id
          | isolated  = (:) (sprites 0 [ivec2 6 1])
          | enclosed  = (:) (sprites 0 animatedLake)
          | otherwise = (:) (sprites 0 [ivec2 4 0])

        sprites life frames = (0, life, frames, tilePos)

        (_gen, bg, _fg) = terrain
        _terrainBg = Terrain.readChunkTile tilePos bg
        terrainSolid = Terrain.readChunkSolid bg tilePos
        terrainSolidNs = Terrain.readChunkSolidNeighbors bg tilePos

        isolated =
          terrainSolidNs == pure False

        enclosed =
          terrainSolidNs == pure True

  where
    render acc = Storable.fromList do
      (tick, _life, frames, tilePos) <- acc

      pure $
        terrainTile tilePos $
          Common.animFrame frames (1 :: Float) (fromInteger tick)

    -- XXX: Terrain.*
    terrainTile (Terrain.TilePos tp) newTile =
      tilesetAt Terrain.tileScale newTile (tp ^* Terrain.tileSize)

animatedLake :: Vector IVec2
animatedLake =
  [ ivec2 4 2
  , ivec2 5 2
  , ivec2 6 2
  ]

animatedSpiral :: Vector IVec2
animatedSpiral =
  [ ivec2 3 43
  , ivec2 4 43
  ]

animatedSparkles :: Vector IVec2
animatedSparkles =
  [ ivec2 1 43
  , ivec2 2 43
  ]

_animatedFires :: Vector IVec2
_animatedFires =
  [ ivec2 0 33
  , ivec2 1 33
  ]

mkTileCursor
  :: RB.MonadMoment m
  => RB.Event tick
  -> RB.Behavior Double
  -> RB.Event Vec2
  -> (Double -> Vec2 -> ann)
  -> m (RB.Event (ann, Vec2))
mkTileCursor tickE globalTime cursorPosE toVar = do
  let
    tileCursorPosE =
      cursorPosE <&>
        emap (fromInteger . round . (/ Terrain.tileSize))
  tileCursorPos <- RB.stepper (1/0) tileCursorPosE

  pure $
    RB.merge tickE cursorPosE @> do
      time <- globalTime
      tilePos <- tileCursorPos
      pure (toVar time tilePos, tilePos)

cursorSprite
  :: (Vec2 -> IVec2 -> Vec2 -> Sprite.InstanceAttrs)
  -> Double
  -> Vec2
  -> Sprite.StorableAttrs
cursorSprite tilesetAt time tilePos =
  Storable.singleton cursorTile
    { Sprite.outline = vec4 2 0 0 1 -- * 0.25
    -- , Sprite.tint    = 0 -- vec4 0 0 0 1
    }
  where
    cursorTile =
      tilesetAt
        Terrain.tileScale
        (Common.animFrame cursorFrames 1.5 time)
        (tilePos ^* Terrain.tileSize)

    cursorFrames =
      [ ivec2 1 26
      , ivec2 4 26
      ]
      -- [ ivec2 1 43
      -- , ivec2 2 43
      -- ]
