module Stage.MainMenu.Render.Imgui
  ( imguiDrawData
  ) where

import RIO.Local

import DearImGui qualified
import DearImGui.Raw qualified as Raw
import Engine.UI.Layout qualified as Layout
import Engine.Worker qualified as Worker
import Foreign (nullPtr, with)
import Foreign.C (withCString)
import Render.ImGui qualified as ImGui
import RIO.Text qualified as Text

import Global.Render (StageFrameRIO)
import Stage.MainMenu.Types (FrameResources(..), RunState(..))

-- import Utils.Dear qualified as Dear

type DrawM a = StageFrameRIO FrameResources RunState a

imguiDrawData :: DrawM DearImGui.DrawData
imguiDrawData = do
  fmap snd $ ImGui.mkDrawData do

    gets rsDebugBox >>= Worker.getOutputData >>= traverse_ debugBox

debugBox :: MonadIO m => (Layout.Box, Text) -> m ()
debugBox (Layout.Box{..}, content) =
  unless (Text.null content) $ liftIO do
    with (withVec2 boxPosition DearImGui.ImVec2) \posPtr ->
      with (DearImGui.ImVec2 0.5 0.5) \pivotPtr ->
        DearImGui.setNextWindowPos posPtr DearImGui.ImGuiCond_Always (Just pivotPtr)

    with (withVec2 boxSize DearImGui.ImVec2) \sizePtr ->
      DearImGui.setNextWindowSize sizePtr DearImGui.ImGuiCond_Always

    bracket open close (`when` action)
  where
    open =
      withCString "##DebugDump" \labelPtr ->
        Raw.begin labelPtr (Just nullPtr) (Just DearImGui.fullscreenFlags)

    close =
      const Raw.end

    action = do
      DearImGui.text content
