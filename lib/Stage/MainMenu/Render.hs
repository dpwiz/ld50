{-# LANGUAGE OverloadedLists #-}

module Stage.MainMenu.Render
  ( updateBuffers
  , recordCommands
  ) where

import RIO.Local

import Engine.Types qualified as Engine
import Engine.Vulkan.Pipeline.Graphics qualified as Graphics
import Engine.Vulkan.Swapchain qualified as Swapchain
import Engine.Worker qualified as Worker
import Render.Basic qualified as Basic
import Render.DescSets.Set0 qualified as Set0
import Render.Draw qualified as Draw
import Render.ForwardMsaa qualified as ForwardMsaa
import Render.ImGui qualified as ImGui
import Resource.Buffer qualified as Buffer
import Vulkan.Core10 qualified as Vk

import Common.Shroud qualified as Shroud
import Global.Render (Pipelines(..), StageFrameRIO)
import Stage.MainMenu.Render.Imgui (imguiDrawData)
import Stage.MainMenu.Types (FrameResources(..), RunState(..))

updateBuffers
  :: RunState
  -> FrameResources
  -> StageFrameRIO FrameResources RunState ()
updateBuffers RunState{..} FrameResources{..} = do
  Set0.observe rsSceneP frScene

  Buffer.observeCoherentResize_ rsNebula frNebula

  Buffer.observeCoherentResize_ rsTileCursor frTileCursor
  Buffer.observeCoherentResize_ rsTileMap frTileMap
  Buffer.observeCoherentResize_ rsSpritesSlow frSpritesSlow
  Buffer.observeCoherentResize_ rsSpritesFast frSpritesFast

  Buffer.observeCoherentResize_ rsStartMessage frStartMessage
  Buffer.observeCoherentResize_ rsDontStartMessage frDontStartMessage
  Buffer.observeCoherentResize_ rsTimeMessage frTimeMessage

recordCommands
  :: Vk.CommandBuffer
  -> FrameResources
  -> Word32
  -> StageFrameRIO FrameResources RunState ()
recordCommands cb _fr@FrameResources{..} imageIndex = do
  (_context, Engine.Frame{fSwapchainResources, fRenderpass, fPipelines}) <- ask
  let Pipelines{..} = fPipelines

  -- XXX: extra update that may or may not reduce hovering latency.
  -- Buffer.observeCoherentResize_ rsTitleMessage frTitleMessage

  dear <- imguiDrawData

  messages <- traverse @[] Worker.readObservedIO
    [ frStartMessage
    , frDontStartMessage
    , frTimeMessage
    ]

  ForwardMsaa.usePass (Basic.rpForwardMsaa fRenderpass) imageIndex cb do
    Swapchain.setDynamicFullscreen cb fSwapchainResources

    Set0.withBoundSet0 frScene (Basic.pWireframe pBasic) cb do
      Graphics.bind cb (Basic.pSprite pBasic) do
        Worker.readObservedIO frNebula >>=
          Draw.quads cb

        Worker.readObservedIO frTileMap >>=
          Draw.quads cb

        Worker.readObservedIO frSpritesSlow >>=
          Draw.quads cb

        Worker.readObservedIO frSpritesFast >>=
          Draw.quads cb

      Graphics.bind cb (Basic.pSpriteOutline pBasic) do
        Worker.readObservedIO frTileCursor >>=
          Draw.quads cb

      Graphics.bind cb (Basic.pEvanwSdf pBasic) $
        traverse_ (Draw.quads cb) messages

    ImGui.draw dear cb

    -- XXX: copypasta
    static <- gets rsShroud
    scene <- gets rsSceneP >>= Worker.getOutputData
    Set0.withBoundSet0 frScene (Basic.pUnlitColoredNoDepth pBasic) cb $
      Graphics.bind cb (Basic.pUnlitColoredNoDepth pBasic) do
        Shroud.draw cb scene static frShroud
