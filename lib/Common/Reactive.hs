{-# LANGUAGE OverloadedLists #-}

module Common.Reactive where

import RIO.Local

import Data.Vector qualified as Vector
import Engine.UI.Layout qualified as Layout
import Render.Samplers qualified as Samplers
import Render.Unlit.Sprite.Model qualified as Sprite

import Global.Resource.Texture qualified as Texture

animFrame :: RealFrac a => Vector p -> a -> a -> p
animFrame frames speed time =
  case Vector.indexM frames ix of
    Nothing ->
      error "zero frames given"
    Just x ->
      x
  where
    ix =
      truncate (time * speed) `rem`
      Vector.length frames

dtFast :: Float
dtFast = 1/60

mkDebugBox
  :: (Show a)
  => Layout.Box
  -> Layout.Box
  -> Maybe a
  -> Maybe (Either () (Layout.Box, Text))
mkDebugBox screen parent = \case
  Nothing ->
    -- Skip update
    mzero
  Just valB ->
    let
      box = Layout.boxFitPlace
        Layout.LeftTop
        (Layout.boxSize parent * vec2 0 1 + vec2 500 0)
        parent
    in
      pure $ Right
        ( box
            { Layout.boxPosition =
                Layout.boxPosition box +
                Layout.boxSize screen / 2
            }
        , showTextPp valB
        )

animateNebula
  :: Texture.Collection Int32
  -> Double
  -> Sprite.StorableAttrs
animateNebula textures timeD =
  [ nebulaBase
      { Sprite.fragRect =
          vec4 offX offY 1 1
      , Sprite.tint =
          vec4 r g b 1
      }
  , nebulaBase
      { Sprite.fragRect =
          vec4 (offX * 0.5 + 0.5) (offY * 0.5 + 0.5) 1 1
      , Sprite.tint =
          vec4 g b r 0.5
      }
  ]
  where
    time = double2Float timeD

    nebulaBase =
      Sprite.fromTexture
        (Samplers.linearRepeat Samplers.indices)
        (Texture.nebula textures)
        4096
        (vec2 0 0)

    offX = cos (time / 120)
    offY = sin (time / 120)
    r = 1 + 0.75 * cos (time / 7)
    g = 1 + 0.75 * sin (time / 5)
    b = 1 + 0.75 * cos (time / 3)
