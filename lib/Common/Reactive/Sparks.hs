{-# LANGUAGE RecursiveDo #-}

module Common.Reactive.Sparks where

import RIO.Local

import Engine.Reactive.Collection qualified as Collection
import Engine.Reactive.Fuse qualified as Fuse
import Engine.Reactive.Motion qualified as Motion
import Reactive.Banana (Behavior, Event)
import Reactive.Banana qualified as RB
import Reactive.Banana.Frameworks (MomentIO)

import Common.Reactive qualified as Common

type Params = Vector IVec2 -- XXX: sprite frames

data New = New
  { params   :: Params
  , position :: RB.Moment (Behavior Point2)
  , fuse     :: Float
  }

data Active = Active
  { params   :: Params             -- ^ Some attributes
  , position :: Behavior Point2    -- ^ The current location of the bullet
  , timer    :: Behavior Float     -- ^ For animation etc.
  , expireE  :: Event Fuse.Expired -- ^ An event that fires whenever a bullet "expires" (bullets only have a finite lifetime)
  }

setup
  :: Event Motion.ClockTick
  -> Event [New]
  -> MomentIO (Behavior [Motion.Located Params (Float, Point2)])
setup tickE spawnE = mdo
  removeE <- Fuse.expire
    changeSetE
    (\Active{expireE} -> expireE)

  changeSetE <- Collection.setup
    spawnE
    removeE
    ( \New{params, fuse, position=positionM} -> do
        (expireE, timer) <- Fuse.countdown tickE fuse
        position <- positionM
        pure Active{..}
    )

  Collection.collect
    changeSetE
    (\Active{position, timer} -> (,) <$> timer <*> position)
    (\Active{params} -> (params,))

explode tickE time pos = do
  take
    (round (4 + sin seed * 2))
    [ spark 1234
    , spark 2345
    , spark 3456
    , spark 4567
    , spark 5678
    , spark 6789
    , spark 7890
    ]
  where
    seed = double2Float time * 10000

    spark seed' = New
      { params =
          [ ivec2 1 43
          , ivec2 2 43
          ]
      , fuse =
          0.5 + 0.25 * sin (seed + seed')
      , position =
          Motion.dirSpeed
            tickE
            pos
            dir
            (16 * 10)
      }
      where
        dir = vec2 (sin dirAngle) (cos dirAngle)
        dirAngle = sin (seed' + seed) * pi

toSprites tilesetAt sparks = do
  (frames, (remaining, Point position)) <- sparks
  pure $ tilesetAt
    (0.5 + vec2 remaining remaining)
    (Common.animFrame frames 8 remaining)
    position
