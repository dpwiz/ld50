module Common.Shroud
  ( State
  , allocate
  , toBlack_
  , toWhite_

  , Resources
  , allocateResources

  , draw
  ) where

import RIO

import Engine.Types (StageRIO)
import Engine.Vulkan.Types (Bound(..), Queues)
import Engine.Worker qualified as Worker
import Geomancy (vec4, withVec4, Vec4, Transform)
import Geomancy.Interpolate qualified as Interpolate
import Geomancy.Vec3 qualified as Vec3
import Geometry.Quad qualified as Quad
import Render.Unlit.Colored.Model qualified as UnlitColored
import Resource.Buffer qualified as Buffer
import RIO.Vector qualified as Vector
import UnliftIO.Resource (ResourceT)
import Vulkan.Core10 qualified as Vk
import Render.DescSets.Set0 (Scene(..))

data State = State
  { source    :: Vec4
  , target    :: Vec4

  , alphaVar :: Worker.Var (Maybe Float)

  , positions :: Buffer.Allocated 'Buffer.Staged Vec3.Packed
  , indices   :: Buffer.Allocated 'Buffer.Staged Word16
  }

data Resources = Resources
  { colors    :: Buffer.Allocated 'Buffer.Coherent Vec4
  , instances :: Buffer.Allocated 'Buffer.Coherent Transform
  }

toBlack_ :: Queues Vk.CommandPool -> Maybe Float -> ResourceT (StageRIO env) State
toBlack_ pools = allocate pools 0 (vec4 0 0 0 1)

toWhite_ :: Queues Vk.CommandPool -> Maybe Float -> ResourceT (StageRIO env) State
toWhite_ pools = allocate pools (vec4 1 1 1 0) 1

allocate :: Queues Vk.CommandPool -> Vec4 -> Vec4 -> Maybe Float -> ResourceT (StageRIO env) State
allocate pools source target initial = do
  alphaVar <- Worker.newVar initial

  positions <- Buffer.createStaged (Just "Shroud.positions") pools Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT 4 $
    Vector.fromList $ map (* 2) $ toList Quad.quadPositions
  Buffer.register positions

  indices <- Buffer.createStaged (Just "Shroud.indices") pools Vk.BUFFER_USAGE_INDEX_BUFFER_BIT 6 $
    Vector.fromList Quad.indicesQuad
  Buffer.register indices

  pure State{..}

allocateResources :: State -> ResourceT (StageRIO rs) Resources
allocateResources static = do
  initial <- Worker.getOutputData $ alphaVar static
  colors <- Buffer.createCoherent (Just "Shroud.colors") Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT 4 $
    Vector.replicate 4 $
      case initial of
        Nothing ->
          0
        Just alpha ->
          Interpolate.linear (source static) (target static) alpha
  Buffer.register colors

  instances <- Buffer.createCoherent (Just "Shroud.instances") Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT 1 [mempty]
  Buffer.register instances

  pure Resources{..}

instance Worker.HasInput State where
  type GetInput State = Maybe Float
  getInput = alphaVar

draw
  :: MonadUnliftIO m
  => Vk.CommandBuffer
  -> Scene
  -> State
  -> Resources
  -> Bound dsl UnlitColored.Vertex Transform m ()
draw cmd Scene{..} State{..} Resources{..} = Bound do
  shroud <- Worker.getOutputData alphaVar
  for_ shroud \alpha -> do
    void $! Buffer.updateCoherent
      ( Vector.replicate 4 $
          withVec4 (Interpolate.linear source target alpha) \r g b a ->
            vec4 (r * a) (g * a) (b * a) a
      )
      colors

    let inverse = sceneInvView <> sceneInvProjection
    void $! Buffer.updateCoherent [inverse] instances

    Vk.cmdBindVertexBuffers cmd 0
      [ Buffer.aBuffer positions
      , Buffer.aBuffer colors
      , Buffer.aBuffer instances
      ]
      [0,0,0]
    Vk.cmdBindIndexBuffer cmd (Buffer.aBuffer indices) 0 Vk.INDEX_TYPE_UINT16
    Vk.cmdDrawIndexed cmd 6 1 0 0 0
