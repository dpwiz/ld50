module Engine.Reactive.Collection where

import RIO.Local

import Data.IntSet qualified as IntSet
import Reactive.Banana (Event)
import Reactive.Banana qualified as RB

setup
  :: RB.MonadMoment m
  => Event [new]
  -> Event IntSet
  -> (new -> RB.Moment active)
  -> m (Event [active])
setup spawnE destroyE activate =
  RB.accumE [] $ RB.unions
    [ newE spawnE activate
    , destroyE <&> \remove old -> do
        (ix, item) <- zip [0..] old
        guard $ IntSet.notMember ix remove
        pure item
    ]

newE
  :: Event [new]
  -> (new -> RB.Moment active)
  -> Event ([active] -> [active])
newE spawnE activate =
  fmap mappend . RB.observeE $
    spawnE <&> traverse activate

collect
  :: RB.MonadMoment m
  => Event [active]
  -> (active -> RB.Behavior b)
  -> (active -> b -> c)
  -> m (RB.Behavior [c])
collect changeSetE getPosition withPosition =
  RB.switchB (pure mempty) $
    changeSetE <&>
      traverse \located ->
        fmap (withPosition located) (getPosition located)
