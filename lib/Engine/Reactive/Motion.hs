module Engine.Reactive.Motion
  ( Located
  , ClockTick(..)
  , spawnTimer

  , static
  , dirSpeed
  , homing
  , linear

  , mixLinear
  ) where

import RIO.Local

import Engine.ReactiveBanana qualified as Network
import Reactive.Banana (Behavior, Event, (<@>))
import Reactive.Banana qualified as RB
import Reactive.Banana.Frameworks qualified as RBF

import Geomancy.Interpolate qualified as Interpolate

newtype ClockTick = ClockTick Float
  deriving (Eq, Show, Num)

spawnTimer
  :: MonadUnliftIO m
  => Float
  -> ResourceT m (RBF.MomentIO (Event ClockTick))
spawnTimer dt = do
  mkTimer <- Network.timer $ round (1e6 * dt)
  pure $ mkTimer <&> \event ->
    event $> ClockTick dt

type Located params a = (params, a)

static
  :: Point2
  -> RB.Moment (Behavior Point2)
static point =
  pure $
    pure point

dirSpeed
  :: Event ClockTick
  -> Point2
  -> Vec2
  -> Float
  -> RB.Moment (Behavior Point2)
dirSpeed stepE origin direction speed =
  RB.accumB origin $
    stepE <&> \(ClockTick dt) position ->
      position .+^ direction ^* (dt * speed)

homing
  :: Event ClockTick
  -> Point2
  -> Behavior Point2
  -> Float
  -> RB.Moment (Behavior Point2)
homing stepE origin target speed =
  RB.accumB origin $
    ((,) <$> target <@> stepE) <&> \(towards, ClockTick dt) current ->
      current .+^ (normalize $ towards .-. current) ^* (dt * speed)

linear
  :: Event ClockTick
  -> Point2
  -> Point2
  -> Float
  -> RB.Moment (Behavior Point2)
linear stepE (Point begin) (Point end) speed =
  fmap (fmap $ Point . snd) $
    RB.accumB (0, begin) $
      stepE <&> \(ClockTick dt) (oldTime, _old) ->
        let
          time = oldTime + dt * speed
        in
          ( time
          , if
            | time <= 0 -> begin
            | time >= 1 -> end
            | otherwise -> Interpolate.linear begin end time
          )

mixLinear
  :: Behavior Point2
  -> Behavior Point2
  -> Behavior Float
  -> Behavior Point2
mixLinear ba bb bt = do
  pa <- ba
  pb <- bb
  t <- bt
  pure $
    if
      | t <= 0 -> pa
      | t >= 1 -> pb
      | otherwise ->
          let
            Point a = pa
            Point b = pb
          in
            Point $ Interpolate.linear a b t
