module Engine.Reactive.Collider where

import RIO.Local

import Data.IntSet qualified as IntSet
import Reactive.Banana ((@>))
import Reactive.Banana qualified as RB

type Collided a = (Int, Point2, a)

-- TODO: unhack
class CollisionCircle a where
  getRadius :: a -> Float
  getPosition :: a -> Point2

setup
  :: ( CollisionCircle a
     , CollisionCircle b
     )
  => RB.Event tick
  -> RB.Behavior [a]
  -> RB.Behavior [b]
  -> ( RB.Event IntSet
     , RB.Event IntSet
     , RB.Event [(Collided a, Collided b)]
     )
setup tickE as bs =
  ( hitsE <&> hitSet . map fst
  , hitsE <&> hitSet . map snd
  , hitsE
  )
  where
    hitsE =
      RB.filterE
        (not . null)
        (tickE @> checkHits)

    checkHits = do
      asNow <- as
      bsNow <- bs
      pure do
        -- XXX: simple MxN range check
        (aix, a) <- zip [0..] asNow
        (bix, b) <- zip [0..] bsNow

        let posA = getPosition a
        let posB = getPosition b
        -- (paramsA, posA@(Point asteroid))
        -- (paramsB, posB@(Point bullet))
        let distance2 = quadrance (posA .-. posB)

        let range = getRadius a - getRadius b
        guard $ distance2 <= range * range

        -- XXX: Emit the colliding parties
        -- XXX: The indices are only valid during their onChangeXxxSet!
        pure
          ( (aix, posA, a)
          , (bix, posB, b)
          )

hitSet :: [Collided params] -> IntSet
hitSet collided = IntSet.fromList do
  (ix, _params, _pos) <- collided
  pure ix
