module Engine.Reactive.Fuse where

import RIO.Local

import Data.IntSet qualified as IntSet
import Reactive.Banana (Behavior, Event)
import Reactive.Banana qualified as RB

import Engine.Reactive.Motion qualified as Motion

data Expired = Expired

countdown
  :: RB.MonadMoment m
  => Event Motion.ClockTick
  -> Float
  -> m (Event Expired, Behavior Float)
countdown tickE fuse = do
  stepE <- RB.accumE fuse $
    tickE <&> \(Motion.ClockTick dt) t -> -- XXX: ugh...
      t - dt

  onExpire <- RB.once $
    RB.filterE (<= 0) stepE $> Expired

  remaining <- RB.stepper fuse stepE

  pure (onExpire, remaining)

expire
  :: RB.MonadMoment m
  => Event [active]
  -> (active -> Event Expired)
  -> m (Event IntSet)
expire changeSetE getExpire =
  RB.switchE RB.never $
    changeSetE <&> \items ->
      foldl' (RB.unionWith mappend) RB.never do
        (ix, item) <- zip [0..] items
        pure $ getExpire item $> IntSet.singleton ix
