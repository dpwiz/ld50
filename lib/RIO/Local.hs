module RIO.Local
  ( module RIO
  , module RIO.Local
  , module Geomancy
  , module RE
  ) where

import RIO

import Control.Monad.Trans.Resource as RE (ResourceT)
import Geomancy
import GHC.Float as RE (double2Float, float2Double)
import RIO.State as RE (gets)
import RIO.Text qualified as Text
import Text.Show.Pretty (ppShow)
import Vulkan.NamedType as RE ((:::))

τ :: Floating a => a
τ = 2 * pi

{-# INLINE showText #-}
showText :: Show a => a -> Text
showText = Text.pack . show

showTextPp :: Show a => a -> Text
showTextPp = Text.pack . ppShow
